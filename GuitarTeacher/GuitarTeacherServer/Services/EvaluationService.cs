﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class EvaluationService : IEvaluationService
    {
        private readonly IRepository<DescriptiveEvaluationDAO> evaluationRepository;
        private readonly IRepository<TeacherDAO> teacherRepository;
        private readonly IRepository<StudentDAO> studentRepository;

        public EvaluationService(IRepository<DescriptiveEvaluationDAO> evaluationRepository,
                                 IRepository<TeacherDAO> teacherRepository,
                                 IRepository<StudentDAO> studentRepository)
        {
            this.evaluationRepository = evaluationRepository;
            this.teacherRepository = teacherRepository;
            this.studentRepository = studentRepository;
        }
        public void AddEvaluation(EvaluationDTO evaluationToAdd, int teacherId, int studentId)
        {
            var newEvaluation = new DescriptiveEvaluationDAO
            {
                EvaluationContent = evaluationToAdd.EvaluationContent,
                StudentId = studentId,
                TeacherId = teacherId,
                GivenDate = evaluationToAdd.GivenDate
            };
            evaluationRepository.Add(newEvaluation);
        }

        public void DeleteEvaluation(int idToDelete)
        {
            var evaluationToDelete = evaluationRepository.Get(idToDelete);
            if (evaluationToDelete != null)
            {
                evaluationRepository.Delete(idToDelete);
            }
            else
            {
                throw new Exception("Evaluation with this id does not exist");
            }
        }

        public void EditEvaluation(EvaluationDTO evaluationToEdit)
        {
            var evaluation = evaluationRepository.Get(evaluationToEdit.Id);
            if (evaluation != null)
            {
                if (evaluation.EvaluationContent != evaluationToEdit.EvaluationContent)
                {
                    evaluation.EvaluationContent = evaluationToEdit.EvaluationContent;
                    evaluationRepository.Update(evaluation);
                }
                else
                {
                    throw new Exception("Nothing to update");
                }
            }
            else
            {
                throw new Exception("Evaluation with this id does not exist");
            }
        }

        public EvaluationsListDTO GetAllEvaluationsForStudent(int studentId)
        {
            var evaluationsList = evaluationRepository.GetAll();
            var evaluationsListDTO = new EvaluationsListDTO();

            foreach(var evaluation in evaluationsList)
            {
                if (evaluation.StudentId == studentId)
                {
                    var evaluationDTO = new EvaluationDTO
                    {
                        EvaluationContent = evaluation.EvaluationContent,
                        Id = evaluation.Id,
                        GivenDate = evaluation.GivenDate
                    };

                    var teacher = teacherRepository.Get(evaluation.TeacherId);

                    evaluationDTO.TeacherName = teacher.FirstName + " " + teacher.LastName;

                    evaluationsListDTO.Evaluations.Add(evaluationDTO);
                }
            }

            return evaluationsListDTO;
        }

        public EvaluationsListDTO GetAllEvaluationsForTeacher(int teacherId)
        {
            var evaluationsList = evaluationRepository.GetAll();
            var evaluationsListDTO = new EvaluationsListDTO();

            foreach (var evaluation in evaluationsList)
            {
                if (evaluation.TeacherId == teacherId)
                {
                    var evaluationDTO = new EvaluationDTO
                    {
                        EvaluationContent = evaluation.EvaluationContent,
                        Id = evaluation.Id,
                        GivenDate = evaluation.GivenDate
                    };

                    var student = studentRepository.Get(evaluation.StudentId);

                    evaluationDTO.StudentName = student.FirstName + " " + student.LastName;

                    evaluationsListDTO.Evaluations.Add(evaluationDTO);
                }
            }

            return evaluationsListDTO;
        }

        public EvaluationDTO GetEvaluation(int idToGet)
        {
            var evaluationToGet = evaluationRepository.Get(idToGet);
            if (evaluationToGet != null)
            {
                var evaluationDTO = new EvaluationDTO
                {
                    EvaluationContent = evaluationToGet.EvaluationContent,
                    GivenDate = evaluationToGet.GivenDate,
                    Id = evaluationToGet.Id
                };
                var student = studentRepository.Get(evaluationToGet.StudentId);
                var teacher = teacherRepository.Get(evaluationToGet.TeacherId);

                evaluationDTO.TeacherName = teacher.FirstName + " " + teacher.LastName;
                evaluationDTO.StudentName = student.FirstName + " " + student.LastName;

                return evaluationDTO;
            }
            else
            {
                throw new Exception("Evaluation with this id does not exist");
            }
        }
    }
}
