﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class TaskService : ITaskService
    {
        private readonly IRepository<TaskDAO> taskRepository;
        private readonly IRepository<TeacherDAO> teacherRepository;
        private readonly IRepository<StudentDAO> studentRepository;

        public TaskService(IRepository<TaskDAO> taskRepository,
                           IRepository<TeacherDAO> teacherRepository,
                           IRepository<StudentDAO> studentRepository)
        {
            this.taskRepository = taskRepository;
            this.teacherRepository = teacherRepository;
            this.studentRepository = studentRepository;
        }
        public void AddTask(TaskDTO taskToAdd, int teacherId, int studentId)
        {
            var newTask = new TaskDAO
            {
                GivenDate = taskToAdd.GivenDate,
                TaskContent = taskToAdd.TaskContent,
                StudentId = studentId,
                TeacherId = teacherId,
                IsRealized = false
            };
            taskRepository.Add(newTask);
        }

        public void DeleteTask(int id)
        {
            var taskToDelete = taskRepository.Get(id);
            if (taskToDelete != null)
            {
                if (taskToDelete.IsRealized == false)
                {
                    taskRepository.Delete(id);
                }
                else
                {
                    throw new Exception("You cannot delete realized task");
                }
            }
            else
            {
                throw new Exception("Task deleted successfully");
            }
        }

        public void EditTask(TaskDTO taskToEdit)
        {
            var editingTask = taskRepository.Get(taskToEdit.Id);

            if (editingTask != null)
            {
                if (editingTask.TaskContent != taskToEdit.TaskContent)
                {
                    editingTask.TaskContent = taskToEdit.TaskContent;
                    taskRepository.Update(editingTask);
                }
                else
                {
                    throw new Exception("Nothing to update!");
                }
            }
            else
            {
                throw new Exception("Task with this ID does not exist");
            }
        }

        public void EvaluateTask(TaskDTO taskToEvaluate)
        {
            var evaluatingTask = taskRepository.Get(taskToEvaluate.Id);
            
            if(evaluatingTask != null)
            {
                if (evaluatingTask.IsRealized == false)
                {
                    evaluatingTask.Note = taskToEvaluate.Note;
                    evaluatingTask.IsRealized = true;

                    taskRepository.Update(evaluatingTask);
                }
                else
                {
                    throw new Exception("You cannot evaluate realized task");
                }
            }
            else
            {
                throw new Exception("Task with this ID does not exist");
            }
        }

        public TasksListDTO GetAllTasksForStudent(int studentId, bool realizedOnly)
        {
            var taskQuery = taskRepository.GetAll();

            TasksListDTO taskList = new TasksListDTO();

            foreach(var task in taskQuery)
            {
                if (task.StudentId == studentId && task.IsRealized == realizedOnly)
                {
                    TaskDTO taskToReturn = new TaskDTO();

                    taskToReturn.GivenDate = task.GivenDate;
                    taskToReturn.Id = task.Id;
                    taskToReturn.TaskContent = task.TaskContent;

                    if (task.IsRealized)
                    {
                        taskToReturn.Note = task.Note;
                    }

                    var teacher = teacherRepository.Get(task.TeacherId);
                    taskToReturn.TeacherName = teacher.FirstName + " " + teacher.LastName;

                    taskList.Tasks.Add(taskToReturn);
                }
            }

            return taskList;
        }

        public TasksListDTO GetAllTasksForTeacher(int teacherId, bool realizedOnly)
        {
            var taskQuery = taskRepository.GetAll();

            TasksListDTO taskList = new TasksListDTO();

            foreach (var task in taskQuery)
            {
                if (task.TeacherId == teacherId&& task.IsRealized == realizedOnly)
                {
                    TaskDTO taskToReturn = new TaskDTO();

                    taskToReturn.GivenDate = task.GivenDate;
                    taskToReturn.Id = task.Id;
                    taskToReturn.TaskContent = task.TaskContent;

                    if (task.IsRealized)
                    {
                        taskToReturn.Note = task.Note;
                    }

                    var student = studentRepository.Get(task.StudentId);
                    taskToReturn.StudentName = student.FirstName + " " + student.LastName;

                    taskList.Tasks.Add(taskToReturn);
                }
            }

            return taskList;
        }

        public TaskDTO GetTask(int id)
        {
            var taskToGet = taskRepository.Get(id);
            if (taskToGet != null)
            {
                var taskToReturn = new TaskDTO
                {
                    GivenDate = taskToGet.GivenDate,
                    TaskContent = taskToGet.TaskContent,
                    Id = taskToGet.Id
                };
                if (taskToGet.IsRealized)
                {
                    taskToReturn.Note = taskToGet.Note;
                }
                var student = studentRepository.Get(taskToGet.StudentId);
                var teacher = teacherRepository.Get(taskToGet.TeacherId);

                taskToReturn.StudentName = student.FirstName + " " + student.LastName;
                taskToReturn.TeacherName = teacher.FirstName + " " + teacher.LastName;

                return taskToReturn;
            }
            else
            {
                throw new Exception("Task deleted successfully");
            }
        }
    }
}
