﻿using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class UserProviderService: IUserProviderService
    {
        private readonly IHttpContextAccessor context;

        public UserProviderService(IHttpContextAccessor context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public int GetUserId()
        {
            try
            {
                return int.Parse(context.HttpContext.User.Claims.First(i => i.Type == "userId").Value);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}
