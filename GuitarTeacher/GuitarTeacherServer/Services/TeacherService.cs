﻿using GuitarTeacherServer.Model;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class TeacherService: ITeacherService
    {
        private readonly IRepository<TeacherDAO> teacherRepository;

        public TeacherService(IRepository<TeacherDAO> teacherRepository)
        {
            this.teacherRepository = teacherRepository;
        }

        public int GetTeacherIdFromUserId(int userId)
        {
            var teachersQuery = teacherRepository.GetAll();

            foreach(var teacher in teachersQuery)
            {
                if (teacher.UserId == userId)
                {
                    return teacher.Id;
                }
            }

            throw new Exception("User is not a teacher!"); 
        }

        public IEnumerable<TeacherDAO> GetTeachers()
        {
            return teacherRepository.GetAll();
        }
        
    }
}
