﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<UserDAO> userRepository;
        private readonly IRepository<TeacherDAO> teacherRepository;
        private readonly IRepository<StudentDAO> studentRepository;
        private readonly PasswordHasher passwordHasher;

        public UserService(IRepository<UserDAO> userRepository,
            IRepository<TeacherDAO> teacherRepository,
            IRepository<StudentDAO> studentRepository)
        {
            this.userRepository = userRepository;
            this.passwordHasher = new PasswordHasher();
            this.teacherRepository = teacherRepository;
            this.studentRepository = studentRepository;

        }
        public int GetUserIdByLogin(string login)
        {
            var users = userRepository.GetAll();

            foreach (var user in users)
            {
                if (user.Login == login)
                {
                    return user.Id;
                }
            }
            throw new Exception("Login is not valid");
        }

        public int GetUseIdrByLoginAndPassword(string login, string password)
        {
            var users = userRepository.GetAll();

            foreach (var user in users)
            {
                if (user.Login == login)
                {
                    if (user.IsActive)
                    {
                        if (passwordHasher.VerifyHashedPassword(user.Password, password) == Microsoft.AspNet.Identity.PasswordVerificationResult.Success)
                        {
                            return user.Id;
                        }
                        else if (passwordHasher.VerifyHashedPassword(user.Password, password) == Microsoft.AspNet.Identity.PasswordVerificationResult.SuccessRehashNeeded)
                        {
                            throw new Exception("Password should be rehashed");
                        }
                        else
                        {
                            throw new Exception("Password is not valid");
                        }
                    }
                    else
                    {
                        throw new Exception("User with this login does not exist");
                    }
                }
            }
            throw new Exception("Login is not valid");
        }

        public bool CheckIfNewUserLoginIsUnique(string newUserLogin)
        {
            IEnumerable<UserDAO> users = userRepository.GetAll();
            foreach (UserDAO user in users)
            {
                if (user.Login == newUserLogin)
                {
                    return false;
                }
            }
            return true;
        }

        public void AddUser(UserDTO newUserDTO)
        {
            if (!CheckIfNewUserLoginIsUnique(newUserDTO.Login))
            {
                throw new Exception("Login is not unique");
            }
            UserDAO userDAO = new UserDAO
            {
                Login = newUserDTO.Login,
                RoleCode = newUserDTO.RoleCode,
                IsActive = true
            };
            userDAO.Password = passwordHasher.HashPassword(newUserDTO.Password);
            userRepository.Add(userDAO);

            if (newUserDTO.RoleCode == "TEA")
            {
                teacherRepository.Add(new TeacherDAO
                {
                    UserId = GetUserIdByLogin(userDAO.Login),
                    FirstName=newUserDTO.FirstName,
                    LastName=newUserDTO.LastName,
                    DateOfBirth=newUserDTO.DateOfBirth
                });
                return;
            }
            else if (newUserDTO.RoleCode == "STU")
            {
                studentRepository.Add(new StudentDAO
                {
                    UserId = GetUserIdByLogin(userDAO.Login),
                    FirstName = newUserDTO.FirstName,
                    LastName = newUserDTO.LastName,
                    DateOfBirth = newUserDTO.DateOfBirth
                });
                return;
            }
            else if (newUserDTO.RoleCode == "ADM")
                return;
            
            throw new Exception("Invalid role");
           
        }

        public UserDTO GetUserById(int userId)
        {
            UserDAO userDAO = userRepository.Get(userId);

            UserDTO userDTO = new UserDTO
            {
                RoleCode = userDAO.RoleCode,
                Login=userDAO.Login,
                Id = userDAO.Id
            };

            if (userDAO.RoleCode == "TEA")
            {
                var teacherQuery = teacherRepository.GetAll();

                foreach(var teacher in teacherQuery)
                {
                    if (teacher.UserId == userId)
                    {
                        userDTO.LastName = teacher.LastName;
                        userDTO.FirstName = teacher.FirstName;
                        userDTO.DateOfBirth = teacher.DateOfBirth;
                        break;
                    }
                }
            }
            else if(userDAO.RoleCode == "STU")
            {
                var studentQuery = studentRepository.GetAll();

                foreach (var student in studentQuery)
                {
                    if (student.UserId == userId)
                    {
                        userDTO.LastName = student.LastName;
                        userDTO.FirstName = student.FirstName;
                        userDTO.DateOfBirth = student.DateOfBirth;
                        break;
                    }
                }
            }

            return userDTO;
        }

        public bool IsTeacher(int userId)
        {
            var teachersQuery = teacherRepository.GetAll();

            foreach(var teacher in teachersQuery)
            {
                if (teacher.UserId == userId)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsStudent(int userId)
        {
            var studentsQuery = studentRepository.GetAll();

            foreach (var student in studentsQuery)
            {
                if (student.UserId == userId)
                {
                    return true;
                }
            }

            return false;
        }
        public void UpdateUser(UserDTO userToUpdate)
        {
            var user = userRepository.Get(userToUpdate.Id);
            bool doUpdate = false;
            bool iDidUpdate = false;

            if (user.Login != userToUpdate.Login)
            {
                if (CheckIfNewUserLoginIsUnique(userToUpdate.Login))
                {
                    user.Login = userToUpdate.Login;
                    userRepository.Update(user);
                    iDidUpdate = true;
                }
                else
                {
                    throw new Exception("Login is not unique!");
                }
            }

            if (user.RoleCode == "TEA")
            {
                var teacherQuery = teacherRepository.GetAll();
                TeacherDAO teacherToUpdate = new TeacherDAO();

                foreach(var teacher in teacherQuery)
                {
                    if (teacher.UserId == userToUpdate.Id)
                    {
                        teacherToUpdate = teacher;
                        break;
                    }
                }
                if (teacherToUpdate.FirstName != userToUpdate.FirstName)
                {
                    teacherToUpdate.FirstName = userToUpdate.FirstName;
                    doUpdate = true;
                }

                if (teacherToUpdate.LastName != userToUpdate.LastName)
                {
                    teacherToUpdate.LastName = userToUpdate.LastName;
                    doUpdate = true;
                }

                if (teacherToUpdate.DateOfBirth != userToUpdate.DateOfBirth)
                {
                    teacherToUpdate.DateOfBirth = userToUpdate.DateOfBirth;
                    doUpdate = true;
                }

                if (doUpdate)
                {
                    teacherRepository.Update(teacherToUpdate);
                    iDidUpdate = true;
                }
            }
            else if (user.RoleCode == "STU")
            {
                var studentQuery = studentRepository.GetAll();
                StudentDAO studentToUpdate = new StudentDAO();

                foreach (var student in studentQuery)
                {
                    if (student.UserId == userToUpdate.Id)
                    {
                        studentToUpdate = student;
                    }
                }

                if (studentToUpdate.FirstName != userToUpdate.FirstName)
                {
                    studentToUpdate.FirstName = userToUpdate.FirstName;
                    doUpdate = true;
                }

                if (studentToUpdate.LastName != userToUpdate.LastName)
                {
                    studentToUpdate.LastName = userToUpdate.LastName;
                    doUpdate = true;
                }

                if (studentToUpdate.DateOfBirth != userToUpdate.DateOfBirth)
                {
                    studentToUpdate.DateOfBirth = userToUpdate.DateOfBirth;
                    doUpdate = true;
                }

                if (doUpdate)
                {
                    studentRepository.Update(studentToUpdate);
                    iDidUpdate = true;
                }

            }
            if (!iDidUpdate)
            {
                throw new Exception("Nothing to update!");
            }
        }

        public void UpdatePassword(LoginDTO newPassword, int userId)
        {
            var user = userRepository.Get(userId);

            if (passwordHasher.VerifyHashedPassword(user.Password, newPassword.Password) == Microsoft.AspNet.Identity.PasswordVerificationResult.Success)
            {
                user.Password = passwordHasher.HashPassword(newPassword.NewPassword);
                userRepository.Update(user);
            }
            else
            {
                throw new Exception("Password is not valid");
            }
        }

    }
}
