﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Model.JoiningTables;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class GroupService : IGroupService
    {
        private readonly IRepository<GroupDAO> groupRepository;
        private readonly IRepository<StudentDAO> studentRepository;
        private readonly IRepository<GroupStudent> groupStudentRepository;

        public GroupService(IRepository<GroupDAO> groupRepository,
                            IRepository<StudentDAO> studentRepository,
                            IRepository<GroupStudent> groupStudentRepository)
        {
            this.groupRepository = groupRepository;
            this.studentRepository = studentRepository;
            this.groupStudentRepository = groupStudentRepository;
        }
        public int CreateGroup(GroupClassDTO groupClass)
        {
            GroupDAO groupToAdd = new GroupDAO();
            groupToAdd = groupRepository.Add(groupToAdd);
            var studentsQuery = studentRepository.GetAll();

            foreach (var student in groupClass.Students)
            {
                groupStudentRepository.Add(new GroupStudent
                {
                    StudentId = student.Id,
                    GroupId = groupToAdd.Id
                });
            }
            return groupToAdd.Id;
        }

    }
}
