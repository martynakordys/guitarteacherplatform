﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class IndividualClassService : IIndividualClassService
    {
        private readonly IRepository<IndividualClassDAO> individualClassRepository;
        private readonly IRepository<TeacherDAO> teacherRepository;
        private readonly IRepository<StudentDAO> studentRepository;

        public IndividualClassService(IRepository<IndividualClassDAO> individualClassRepository,
            IRepository<TeacherDAO> teacherRepository,
            IRepository<StudentDAO> studentRepository)
        {
            this.individualClassRepository = individualClassRepository;
            this.teacherRepository = teacherRepository;
            this.studentRepository = studentRepository;
        }
        public void AddIndividualClass(IndividualClassDTO classDTO, int teacherId, int studentId)
        {

            var classDAO = new IndividualClassDAO
            {
                DurationMin = classDTO.DurationTime,
                DateOfClasses = classDTO.DateOfClasses,
                TeacherId = teacherId,
                StudentId = studentId,
                IsRealized = false
            };

            individualClassRepository.Add(classDAO);

        }

        public IndividualClassDTO GetIndividualClass(int classId)
        { 

            IndividualClassDTO classDTO = new IndividualClassDTO();
            var classesQuery = individualClassRepository.GetAll();

            foreach(var indClass in classesQuery)
            {
                if (indClass.Id == classId)
                {
                    var teacherQuery = teacherRepository.GetAll();
                    string teacherName="", studentName="";
                    foreach(var teacher in teacherQuery)
                    {
                        if (teacher.Id == indClass.TeacherId)
                        {
                            teacherName = teacher.FirstName + " " + teacher.LastName;
                            break;
                        }
                    }

                    var studentQuery = studentRepository.GetAll();
                    foreach(var student in studentQuery)
                    {
                        if(student.Id==indClass.StudentId)
                        {
                            studentName = student.FirstName + " " + student.LastName;
                            break;
                        }    
                    }

                    classDTO.DateOfClasses = indClass.DateOfClasses;
                    classDTO.DurationTime = indClass.DurationMin;
                    classDTO.StudentName = studentName;
                    classDTO.TeacherName = teacherName;
                    
                    return classDTO;
                }
            }
            throw new Exception("Individual classes with this ID does not exist");
        }

        public IndividualClassesListDTO GetIndividualClassesForTeacher(int teacherId, bool unrealizedOnly)
        {
            IndividualClassesListDTO classListDTO = new IndividualClassesListDTO();

            var classQuery = individualClassRepository.GetAll();
            var studentsQuery = studentRepository.GetAll();

            foreach(var indClass in classQuery)
            {
                if(indClass.TeacherId==teacherId&&indClass.IsRealized!=unrealizedOnly)
                {
                    var studentName = "";
                    foreach(var student in studentsQuery)
                    {
                        if(student.Id == indClass.StudentId)
                        {
                            studentName = student.FirstName + " " + student.LastName;
                            IndividualClassDTO tempIndClass = new IndividualClassDTO
                            {
                                Id = indClass.Id,
                                DateOfClasses = indClass.DateOfClasses,
                                DurationTime = indClass.DurationMin,
                                StudentName = studentName
                            };

                            if (indClass.IsRealized)
                            {
                                tempIndClass.Topic = indClass.Topic;
                            }

                            classListDTO.IndividualClasses.Add(tempIndClass);
                            break;
                        }    
                    }
                }    
            }

            return classListDTO;
        }

        public IndividualClassesListDTO GetIndividualClassesForStudent(int studentId, bool unrealizedOnly)
        {
            IndividualClassesListDTO classListDTO = new IndividualClassesListDTO();

            var classQuery = individualClassRepository.GetAll();
            var teachersQuery = teacherRepository.GetAll();

            foreach(var indClass in classQuery)
            {
                if (indClass.StudentId == studentId)
                {
                    var teacherName = "";
                    foreach(var teacher in teachersQuery)
                    {
                        if (indClass.TeacherId == teacher.Id && indClass.IsRealized != unrealizedOnly)
                        {
                            teacherName = teacher.FirstName + " " + teacher.LastName;
                            IndividualClassDTO tempIndClass = new IndividualClassDTO
                            {
                                Id = indClass.Id,
                                DateOfClasses = indClass.DateOfClasses,
                                DurationTime = indClass.DurationMin,
                                TeacherName = teacherName
                            };

                            if (indClass.IsRealized)
                            {
                                tempIndClass.Topic = indClass.Topic;
                            }

                            classListDTO.IndividualClasses.Add(tempIndClass);

                            break;
                        }
                    }
                }
            }

            return classListDTO;
        }

        public void RealizeIndividualClass(int classId, string topic)
        {
            var classToRealize = individualClassRepository.Get(classId);

            if (classToRealize != null)
            {
                classToRealize.Topic = topic;
                classToRealize.IsRealized = true;

                individualClassRepository.Update(classToRealize);
            }
            else
            {
                throw new Exception("Wrong class id!");
            }
        }

        public void EditIndividualClass(IndividualClassDTO updatedClass)
        {
            var classToUpdate = individualClassRepository.Get(updatedClass.Id);

            if (classToUpdate != null)
            {
                if (classToUpdate.IsRealized == false)
                {
                    bool doUpdate = false;
                    if (classToUpdate.DateOfClasses != updatedClass.DateOfClasses)
                    {
                        classToUpdate.DateOfClasses = updatedClass.DateOfClasses;
                        doUpdate = true;
                    }
                    if (classToUpdate.DurationMin != updatedClass.DurationTime)
                    {
                        classToUpdate.DurationMin = updatedClass.DurationTime;
                        doUpdate = true;
                    }

                    if (doUpdate)
                    {
                        individualClassRepository.Update(classToUpdate);
                    }
                    else
                    {
                        throw new Exception("Nothing to update");
                    }

                }
                else
                {
                    throw new Exception("You cannot update realized class");
                }
            }
        }

        public void DeleteIndividualClass(int id)
        {
            var classToDelete = individualClassRepository.Get(id);

            if (classToDelete != null)
            {
                individualClassRepository.Delete(id);
            }
            else
            {
                throw new Exception("Nothing to delete!");
            }
        }

    }
}
