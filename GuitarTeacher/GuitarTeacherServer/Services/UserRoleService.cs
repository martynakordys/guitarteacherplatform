﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Repositories;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class UserRoleService : IUserRoleService
    {
        private readonly IUserRoleRepository userRoleRepository;

        public UserRoleService(IUserRoleRepository userRoleRepository)
        {
            this.userRoleRepository = userRoleRepository;
        }
        public UserRolesListDTO GetAvailalbeRoles()
        {
            UserRolesListDTO roleListDTO = new UserRolesListDTO();
            var userRolesQuery = userRoleRepository.GetAll();
            foreach (var userRole in userRolesQuery)
            {
                if (userRole.Name != "Admin")
                {
                    roleListDTO.UserRoles.Add(new UserRoleDTO { 
                        Code=userRole.Code,
                        Name=userRole.Name
                    });
                }
            }
            return roleListDTO;
        }

        public string GetRoleNameByCode(string code)
        {
            var userRolesQuery = userRoleRepository.GetAll();
            foreach (var userRole in userRolesQuery)
            {
                if (userRole.Code == code)
                    return userRole.Name;
            }
            return null;
        }
    }
}
