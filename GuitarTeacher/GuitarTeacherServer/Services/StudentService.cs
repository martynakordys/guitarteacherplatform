﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class StudentService : IStudentService
    {
        private readonly IRepository<StudentDAO> studentRepository;
        private readonly IRepository<UserDAO> userRepository;

        public StudentService(IRepository<StudentDAO> studentRepository,
            IRepository<UserDAO> userRepository)
        {
            this.studentRepository = studentRepository;
            this.userRepository = userRepository;
        }
        public int GetIdByUserLogin(string login)
        {
            var userQuery = userRepository.GetAll();
            var studentQuery = studentRepository.GetAll();
            int userId = 0;
            foreach(var user in userQuery)
            {
                if(user.Login == login)
                {
                    userId = user.Id;
                    break;
                }    
            }

            foreach (var student in studentQuery)
            {
                if (student.UserId == userId)
                {
                    return student.Id;
                }
            }

            throw new Exception("Student with this login does not exist");
        }
        public int GetStudentIdFromUserId(int userId)
        {
            var studentsQuery = studentRepository.GetAll();

            foreach (var student in studentsQuery)
            {
                if (student.UserId == userId)
                {
                    return student.Id;
                }
            }

            throw new Exception("User is not a student!");
        }
        public StudentDTO GetStudentById(int id)
        {
            var student = studentRepository.Get(id);

            if (student != null)
            {
                var user = userRepository.Get(student.UserId);
                var studentDTO = new StudentDTO
                {
                    Id = student.Id,
                    Login = user.Login,
                    Name = student.FirstName + " " + student.LastName
                };
                return studentDTO;
            }
            else
            {
                throw new Exception("Student with this ID does not exist");
            }
        }

    }
}
