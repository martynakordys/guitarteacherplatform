﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Model.JoiningTables;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class ChordService: IChordService
    {
        private readonly IRepository<ChordDAO> chordRepository;
        private readonly IRepository<PictureDAO> pictureRepository;
        private readonly IRepository<StudentChord> studentChordRepository;
        private readonly IRepository<TeacherDAO> teacherRepository;
        private readonly IRepository<StudentDAO> studentRepository;
        private readonly IRepository<UserDAO> userRepository;

        public ChordService(IRepository<ChordDAO> chordRepository,
                            IRepository<PictureDAO> pictureRepository,
                            IRepository<StudentChord> studentChordRepository,
                            IRepository<TeacherDAO> teacherRepository,
                            IRepository<StudentDAO> studentRepository,
                            IRepository<UserDAO> userRepository)
        {
            this.chordRepository = chordRepository;
            this.pictureRepository = pictureRepository;
            this.studentChordRepository = studentChordRepository;
            this.teacherRepository = teacherRepository;
            this.studentRepository = studentRepository;
            this.userRepository = userRepository;
        }

        public void AddChord(ChordDTO chordToAdd, Bitmap chordPicture, int teacherId)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                chordPicture.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                var chordPictrueByteArray = ms.ToArray();
                var pictureEntity = pictureRepository.Add(new PictureDAO 
                { 
                    PictureData = chordPictrueByteArray 
                });

                var newChord = new ChordDAO
                {
                    IsPublic = chordToAdd.IsPublic,
                    Name = chordToAdd.Name,
                    TeacherId = teacherId,
                    PictureId = pictureEntity.Id,
                    UploadDate = DateTime.Now
                };

                chordRepository.Add(newChord);
            }
        }

        public Bitmap CreateChordImage(ChordSchemeDTO chordScheme)
        {
            var chordbackground = Image.FromFile("Resources/Images/chord.png");

            using(var g = Graphics.FromImage(chordbackground))
            {
                if (chordScheme.FretNumber != 1)
                {
                    var fretnumberpath = "Resources/Images/" + chordScheme.FretNumber.ToString() + ".png";
                    var fretNumberImage = Image.FromFile(fretnumberpath);
                    g.DrawImage(fretNumberImage, 0, 60);
                }

                for (int i = 0; i < 6; i++)
                {
                    if (chordScheme.StringsSymbols[i] == 'x')
                    {
                        var stringSymbolCross = Image.FromFile("Resources/Images/cross.png");
                        g.DrawImage(stringSymbolCross, 40 * (i + 1), 0);
                    }
                    else if (chordScheme.StringsSymbols[i] == 'o')
                    {
                        var stringSymbolCircle = Image.FromFile("Resources/Images/circle.png");
                        g.DrawImage(stringSymbolCircle, 40 * (i + 1), 0);
                    }
                }

                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        if (chordScheme.FingersSymbols[i][j] == 'f')
                        {
                            var fingerSymbol = Image.FromFile("Resources/Images/finger.png");
                            g.DrawImage(fingerSymbol, 40 * (j + 1), 60 * (i + 1));
                        }
                        else if (chordScheme.FingersSymbols[i][j] == 'l')
                        {
                            var barreLeftEnd = Image.FromFile("Resources/Images/barreleftend.png");
                            g.DrawImage(barreLeftEnd, 40 * (j + 1), 60 * (i + 1));
                        }
                        else if (chordScheme.FingersSymbols[i][j] == 'r')
                        {
                            var barreRightEnd = Image.FromFile("Resources/Images/barrerightend.png");
                            g.DrawImage(barreRightEnd, 40 * (j + 1), 60 * (i + 1));
                        }
                        else if (chordScheme.FingersSymbols[i][j] == 'm')
                        {
                            var barreMiddle = Image.FromFile("Resources/Images/barremiddle.png");
                            g.DrawImage(barreMiddle, 40 * (j + 1), 60 * (i + 1));
                        }
                    }
                }
            }

            var finalBitmap = new Bitmap(300, 440);

            using (var g = Graphics.FromImage(finalBitmap))
            using (SolidBrush brush = new SolidBrush(Color.White))
            {
                g.FillRectangle(brush, 0, 0, 300, 440);
                g.DrawImage(chordbackground, 0, 40);
            }

            return finalBitmap;

        }

        public ChordsListDTO GetAllChordsForTeacher(int teacherId)
        {
            var chordsList = new ChordsListDTO();
            var chordsQuery = chordRepository.GetAll();

            foreach (var chord in chordsQuery)
            {
                if (chord.TeacherId == teacherId)
                {
                    var chordPicture = pictureRepository.Get(chord.PictureId);

                    var chordDTO = new ChordDTO
                    {
                        Id = chord.Id,
                        Name = chord.Name,
                        IsPublic = chord.IsPublic,
                        ChordImage = chordPicture.PictureData,
                        UploadDate = chord.UploadDate
                    };

                    chordsList.Chords.Add(chordDTO);
                }
            }

            return chordsList;
        }

        public ChordsListDTO GetAllChordsForStudent(int studentId)
        {
            var chordListDTO = new ChordsListDTO();
            var studentChordsQuery = studentChordRepository.GetAll();

            foreach (var studentChord in studentChordsQuery)
            {
                if (studentChord.StudentId == studentId)
                {
                    var chord = chordRepository.Get(studentChord.ChordId);

                    var chordPicture = pictureRepository.Get(chord.PictureId);

                    var teacher = teacherRepository.Get(chord.TeacherId);

                    var teacherName = teacher.FirstName + " " + teacher.LastName;

                    var chordDTO = new ChordDTO
                    {
                        Id = chord.Id,
                        Name = chord.Name,
                        IsPublic = chord.IsPublic,
                        ChordImage = chordPicture.PictureData,
                        TeacherName = teacherName,
                        UploadDate = chord.UploadDate
                    };

                    chordListDTO.Chords.Add(chordDTO);
                }
            }

            return chordListDTO;

        }

        public ChordDTO GetChord(int chordId)
        {
            var chordToGet = chordRepository.Get(chordId);

            if (chordToGet != null)
            {
                var studentChordQuery = studentChordRepository.GetAll();
                var chordPicture = pictureRepository.Get(chordToGet.PictureId);

                ChordDTO chordDTO = new ChordDTO
                {
                    Id = chordToGet.Id,
                    Name = chordToGet.Name,
                    UploadDate = chordToGet.UploadDate,
                    ChordImage = chordPicture.PictureData,
                    IsPublic = chordToGet.IsPublic
                };

                foreach(var studentChord in studentChordQuery)
                {
                    if (studentChord.ChordId == chordToGet.Id)
                    {
                        var student = studentRepository.Get(studentChord.StudentId);
                        var user = userRepository.Get(student.UserId);

                        var newStudent = new StudentDTO
                        {
                            Id = student.Id,
                            Login = user.Login,
                            Name = student.FirstName + " " + student.LastName
                        };
                        chordDTO.SharingStudents.Add(newStudent);
                    }
                }

                return chordDTO;
            }
            else
            {
                throw new Exception("Chord with this Id does not exist!");
            }
        }

        public ChordsListDTO GetAllPublicChords()
        {
            var chordListDTO = new ChordsListDTO();
            var chordsQuery = chordRepository.GetAll();

            foreach (var chord in chordsQuery)
            {
                if (chord.IsPublic == true)
                {
                    var chordPicture = pictureRepository.Get(chord.PictureId);

                    var teacher = teacherRepository.Get(chord.TeacherId);

                    var teacherName = teacher.FirstName + " " + teacher.LastName;

                    var chordDTO = new ChordDTO
                    {
                        Id = chord.Id,
                        Name = chord.Name,
                        IsPublic = chord.IsPublic,
                        ChordImage = chordPicture.PictureData,
                        TeacherName = teacherName,
                        UploadDate = chord.UploadDate
                    };

                    chordListDTO.Chords.Add(chordDTO);
                }
            }

            return chordListDTO;

        }

        public void DeleteChord(int chordId)
        {
            var chordToDelete = chordRepository.Get(chordId);

            if (chordToDelete != null)
            {
                var studentChordQuery = studentChordRepository.GetAll();
                var entitiesToRemove = new List<StudentChord>();

                foreach(var studentChord in studentChordQuery)
                {
                    if (studentChord.ChordId == chordId)
                    {
                        entitiesToRemove.Add(studentChord);
                    }
                }

                foreach(var entity in entitiesToRemove)
                {
                    studentChordRepository.DeleteEntity(entity);
                }

                chordRepository.Delete(chordToDelete.Id);
            }
            else
            {
                throw new Exception("Nothing to delete!");
            }
        }

        public void ShareChord(ShareObjectDTO shareObject)
        {
            var chordToShare = chordRepository.Get(shareObject.ObjectId);

            if (chordToShare != null)
            {
                bool nothingToShare = true;
                var studentChordQuery = studentChordRepository.GetAll();
                var entitiesToRemove = new List<StudentChord>();

                foreach(var studentChord in studentChordQuery)
                {
                    if (studentChord.ChordId == chordToShare.Id)
                    {
                        if (shareObject.StudentsIds.Contains(studentChord.StudentId))
                        {
                            shareObject.StudentsIds.Remove(studentChord.StudentId);
                        }
                        else
                        {
                            entitiesToRemove.Add(studentChord);
                            nothingToShare = false;
                        }
                    }
                }
            
                foreach(var entity in entitiesToRemove)
                {
                    studentChordRepository.DeleteEntity(entity);
                }

                if (shareObject.StudentsIds.Count != 0)
                {
                    nothingToShare = false;
                    foreach(var id in shareObject.StudentsIds)
                    {
                        studentChordRepository.Add(new StudentChord
                        {
                            ChordId = shareObject.ObjectId,
                            StudentId = id
                        });
                    }
                }

                if (nothingToShare)
                {
                    throw new Exception("Nothing to share!");
                }
            }
            else
            {
                throw new Exception("Chord with this Id does not exist!");
            }
        }


    }
}
