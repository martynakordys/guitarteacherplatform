﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Model.JoiningTables;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Services
{
    public class GroupClassService : IGroupClassService
    {
        private readonly IRepository<GroupClassDAO> groupClassRepository;
        private readonly IRepository<StudentDAO> studentRepository;
        private readonly IRepository<GroupStudent> groupStudentRepository;
        private readonly IRepository<UserDAO> userRepository;
        private readonly IRepository<AttendenceDAO> attendenceRepository;
        private readonly IRepository<TeacherDAO> teacherRepository;

        public GroupClassService(IRepository<GroupClassDAO> groupClassRepository,
                                 IRepository<StudentDAO> studentRepository,
                                 IRepository<GroupStudent> groupStudentRepository,
                                 IRepository<UserDAO> userRepository,
                                 IRepository<AttendenceDAO> attendenceRepository,
                                 IRepository<TeacherDAO> teacherRepository)
        {
            this.groupClassRepository = groupClassRepository;
            this.studentRepository = studentRepository;
            this.groupStudentRepository = groupStudentRepository;
            this.userRepository = userRepository;
            this.attendenceRepository = attendenceRepository;
            this.teacherRepository = teacherRepository;
        }
        public void AddGroupClass(GroupClassDTO groupClass, int teacherId, int groupId)
        {
            groupClassRepository.Add(new GroupClassDAO
            {
                DateOfClasses = groupClass.DateOfClass,
                DurationMin = groupClass.Duration,
                GroupId = groupId,
                TeacherId = teacherId,
                IsRealized = false
            });
        }

        public GroupClassDTO GetGroupClass(int classId)
        {

            GroupClassDTO classDTO = new GroupClassDTO();
            var classToGet = groupClassRepository.Get(classId);

            if (classToGet!=null)
            {
                var teacher = teacherRepository.Get(classToGet.TeacherId);
                classDTO.TeacherName = teacher.FirstName + " " + teacher.LastName;

                classDTO.DateOfClass = classToGet.DateOfClasses;
                classDTO.Duration = classToGet.DurationMin;
                classDTO.Id = classToGet.Id;

                if(classToGet.IsRealized)
                {
                    classDTO.Topic = classToGet.Topic;
                }

                var groupStudentQuery = groupStudentRepository.GetAll();
                var attendenceQuery = attendenceRepository.GetAll();

                foreach (var groupStudent in groupStudentQuery)
                {
                    if (groupStudent.GroupId == classToGet.GroupId)
                    {
                        var student = studentRepository.Get(groupStudent.StudentId);
                        var user = userRepository.Get(student.UserId);

                        var newStudent = new StudentDTO
                        {
                            Id = student.Id,
                            Login = user.Login,
                            Name = student.FirstName + " " + student.LastName
                        };

                        if (classToGet.IsRealized)
                        {
                            foreach (var attendence in attendenceQuery)
                            {
                                if (attendence.GroupClassId == classId && attendence.StudentId == student.Id)
                                {
                                    newStudent.IsPresent = attendence.IsPresent;
                                }
                            }
                        }

                        classDTO.Students.Add(newStudent);
                    }
                }

                return classDTO;
            }
            else
            {
                throw new Exception("Group classes with this ID does not exist");
            }
        }

        public void DeleteGroupClass(int idToDelete)
        {
            var classToDelete = groupClassRepository.Get(idToDelete);

            if (classToDelete != null)
            {
                var studentGroupQuery = groupStudentRepository.GetAll();
                List<GroupStudent> toRemove = new List<GroupStudent>();

                foreach(var studentGroupRecord in studentGroupQuery)
                {
                    if (studentGroupRecord.GroupId == classToDelete.Id)
                    {
                        toRemove.Add(studentGroupRecord);
                    }
                }

                foreach(var entityToDelete in toRemove)
                {
                    groupStudentRepository.DeleteEntity(entityToDelete);
                }

                groupClassRepository.Delete(classToDelete.Id);
            }
            else
            {
                throw new Exception("Nothing to delete!");
            }
        }

        public void EditGroupClass(GroupClassDTO updatedClass)
        {
            var classToUpdate = groupClassRepository.Get(updatedClass.Id);

            if (classToUpdate != null)
            {
                if (classToUpdate.IsRealized == false)
                {
                    bool doUpdate = false;
                    if (classToUpdate.DateOfClasses != updatedClass.DateOfClass)
                    {
                        classToUpdate.DateOfClasses = updatedClass.DateOfClass;
                        doUpdate = true;
                    }
                    if (classToUpdate.DurationMin != updatedClass.Duration)
                    {
                        classToUpdate.DurationMin = updatedClass.Duration;
                        doUpdate = true;
                    }

                    var groupStudentsQuery = groupStudentRepository.GetAll();
                    var toRemoveList = new List<string>();
                    var entitiesToRemove = new List<GroupStudent>();
                    bool removeEntity = true;

                    foreach(var groupStudent in groupStudentsQuery)
                    {
                        if (groupStudent.GroupId == classToUpdate.Id)
                        {
                            removeEntity = true;
                            foreach (var student in updatedClass.Students)
                            {
                                if (groupStudent.StudentId == student.Id)
                                {
                                    toRemoveList.Add(student.Login);
                                    removeEntity = false;
                                }
                            }
                            if (removeEntity)
                            {
                                entitiesToRemove.Add(groupStudent);
                                doUpdate = true;
                            }
                        }
                    }
                    updatedClass.Students.RemoveAll(t => toRemoveList.Contains(t.Login));
                    foreach(var entityToRemove in entitiesToRemove)
                    {
                        groupStudentRepository.DeleteEntity(entityToRemove);
                    }
                    if (updatedClass.Students.Count != 0)
                    {
                        doUpdate = true;
                        foreach(var studentToAdd in updatedClass.Students)
                        {
                            groupStudentRepository.Add(new GroupStudent
                            {
                                GroupId = updatedClass.Id,
                                StudentId = studentToAdd.Id
                            });
                        }
                    }

                    if (doUpdate)
                    {
                        groupClassRepository.Update(classToUpdate);
                    }
                    else
                    {
                        throw new Exception("Nothing to update");
                    }

                }
                else
                {
                    throw new Exception("You cannot update realized class");
                }
            }
            else
            {
                throw new Exception("Class with this id does not exist!");
            }
        }

        public GroupClassesListDTO GetAllGroupClassesForTeacher(int teacherId, bool unrealizedOnly)
        {
            GroupClassesListDTO classListDTO = new GroupClassesListDTO();

            var classQuery = groupClassRepository.GetAll();
            var studentsGroupsQuery = groupStudentRepository.GetAll();

            foreach (var groupClass in classQuery)
            {
                if (groupClass.TeacherId == teacherId && groupClass.IsRealized != unrealizedOnly)
                {
                    GroupClassDTO groupClassDTO = new GroupClassDTO {
                        Id = groupClass.Id,
                        DateOfClass=groupClass.DateOfClasses,
                        Duration=groupClass.DurationMin
                    };

                    if (groupClass.IsRealized)
                    {
                        groupClassDTO.Topic = groupClass.Topic;
                    }

                    foreach(var groupStudent in studentsGroupsQuery)
                    {
                        if(groupStudent.GroupId== groupClass.Id)
                        {
                            var student = studentRepository.Get(groupStudent.StudentId);
                            var user = userRepository.Get(student.UserId);

                            var studentToAdd = new StudentDTO
                            {
                                Id = student.Id,
                                Login = user.Login,
                                Name = student.FirstName + " " + student.LastName
                            };

                            if (groupClass.IsRealized)
                            {
                                var attendenceQuery = attendenceRepository.GetAll();

                                foreach(var attendence in attendenceQuery)
                                {
                                    if(attendence.GroupClassId== groupClass.Id && attendence.StudentId == student.Id)
                                    {
                                        studentToAdd.IsPresent = attendence.IsPresent;
                                    }
                                }
                            }

                            groupClassDTO.Students.Add(studentToAdd);
                        }
                    }
                    classListDTO.GroupClasses.Add(groupClassDTO);
                }
            }

            return classListDTO;
        }


        public GroupClassesListDTO GetAllGroupClassesForStudent(int studentId, bool unrealizedOnly)
        {
            GroupClassesListDTO classListDTO = new GroupClassesListDTO();

            var studentsGroupsQuery = groupStudentRepository.GetAll();
            var currentStudentGroupsIds = new List<int>();

            foreach(var studentGroup in studentsGroupsQuery)
            {
                if (studentGroup.StudentId == studentId)
                {
                    currentStudentGroupsIds.Add(studentGroup.GroupId);
                }
            }

            foreach (var groupId in currentStudentGroupsIds)
            {
                var groupClass = groupClassRepository.Get(groupId);

                if(groupClass.IsRealized!= unrealizedOnly)
                {
                    GroupClassDTO groupClassDTO = new GroupClassDTO
                    {
                        Id = groupClass.Id,
                        DateOfClass = groupClass.DateOfClasses,
                        Duration = groupClass.DurationMin
                    };

                    var teacher = teacherRepository.Get(groupClass.TeacherId);

                    groupClassDTO.TeacherName = teacher.FirstName + " " + teacher.LastName;

                    if (groupClass.IsRealized)
                    {
                        groupClassDTO.Topic = groupClass.Topic;
                    }

                    foreach (var groupStudent in studentsGroupsQuery)
                    {
                        if (groupStudent.GroupId == groupClass.Id)
                        {
                            var student = studentRepository.Get(groupStudent.StudentId);
                            var user = userRepository.Get(student.UserId);

                            var studentToAdd = new StudentDTO
                            {
                                Id = student.Id,
                                Login = user.Login,
                                Name = student.FirstName + " " + student.LastName
                            };

                            if (groupClass.IsRealized)
                            {
                                var attendenceQuery = attendenceRepository.GetAll();

                                foreach (var attendence in attendenceQuery)
                                {
                                    if (attendence.GroupClassId == groupClass.Id && attendence.StudentId == student.Id)
                                    {
                                        studentToAdd.IsPresent = attendence.IsPresent;
                                    }
                                }
                            }

                            groupClassDTO.Students.Add(studentToAdd);
                        }
                    }

                    classListDTO.GroupClasses.Add(groupClassDTO);
                }
            }

            return classListDTO;
        }

        public void RealizeGroupClass(AttendenceDTO attendence)
        {
            var classToRealize = groupClassRepository.Get(attendence.GroupClassId);

            if (classToRealize != null)
            {
                classToRealize.Topic = attendence.Topic;
                classToRealize.IsRealized = true;
                groupClassRepository.Update(classToRealize);

                foreach (var presentStudent in attendence.PresentStudentsIds)
                {
                    attendenceRepository.Add(new AttendenceDAO
                    {
                        IsPresent = true,
                        GroupClassId = attendence.GroupClassId,
                        StudentId = presentStudent
                    });
                }

                foreach (var absentStudent in attendence.AbsentStudentsIds)
                {
                    attendenceRepository.Add(new AttendenceDAO
                    {
                        IsPresent = false,
                        GroupClassId = attendence.GroupClassId,
                        StudentId = absentStudent
                    });
                }
            }
            else
            {
                throw new Exception("Wrong class id!");
            }
            
        }

        public void UpdateAttendence(AttendenceDTO attendenceToUpdate)
        {
            var classToUpdate = groupClassRepository.Get(attendenceToUpdate.GroupClassId);

            if (classToUpdate != null)
            {
                var attendenceQuery = attendenceRepository.GetAll();
                var entitiesToUpdate = new List<AttendenceDAO>();

                foreach (var attendence in attendenceQuery)
                {
                    bool studentFound = false;

                    if (attendence.GroupClassId == classToUpdate.Id)
                    {
                        foreach(var id in attendenceToUpdate.PresentStudentsIds)
                        {
                            if (id == attendence.StudentId)
                            {
                                if (attendence.IsPresent == false)
                                {
                                    attendence.IsPresent = true;
                                    entitiesToUpdate.Add(attendence);
                                }
                                studentFound = true;
                                break;
                            }
                        }

                        if (!studentFound)
                        {
                            foreach (var id in attendenceToUpdate.AbsentStudentsIds)
                            {
                                if (id == attendence.StudentId)
                                {
                                    if (attendence.IsPresent == true)
                                    {
                                        attendence.IsPresent = false;
                                        entitiesToUpdate.Add(attendence);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                if (entitiesToUpdate.Count == 0)
                {
                    throw new Exception("Nothing to update");
                }

                foreach(var entity in entitiesToUpdate)
                {
                    attendenceRepository.Update(entity);
                }

            }
            else
            {
                throw new Exception("Wrong class id!");
            }

        }


    }
}
