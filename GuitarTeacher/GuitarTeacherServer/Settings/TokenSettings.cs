﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Settings
{
    public interface ITokenSettings
    {
        string SecretKey { get; set; }
    }
    public class TokenSettings: ITokenSettings
    {
        public string SecretKey { get; set; } = "mdewhiusbqueh123nfjke";
    }
}
