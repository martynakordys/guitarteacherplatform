﻿using GuitarTeacherServer.Model;
using GuitarTeacherServer.Model.Base;
using GuitarTeacherServer.Model.JoiningTables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer
{
    public class GuitarDBContext : DbContext
    {
        public DbSet<TeacherDAO> Teachers { get; set; }
        public DbSet<IndividualClassDAO> IndividualClasses { get; set; }
        public DbSet<UserDAO> Users { get; set; }
        public DbSet<GroupClassDAO> GroupClasses { get; set; }
        public DbSet<UserRoleDAO> UserRoles { get; set; }
        public DbSet<TabulatureDAO> Tabulatures { get; set; }
        public DbSet<ChordDAO> Chords { get; set; }
        public DbSet<StudentDAO> Students { get; set; }
        public DbSet<GroupDAO> Groups { get; set; }
        public DbSet<TaskDAO> Tasks { get; set; }
        public DbSet<DescriptiveEvaluationDAO> DescriptiveEvaluations { get; set; }
        public DbSet<AttendenceDAO> Attendences { get; set; }
        public DbSet<PictureDAO> Pictures { get; set; }

        //joiningTables

        public DbSet<GroupStudent> GroupStudents { get; set; }
        public DbSet<StudentChord> StudentChords { get; set; }
        public DbSet<StudentTabulature> StudentTabulatures { get; set; }


        public GuitarDBContext(DbContextOptions<GuitarDBContext> options): base(options) {}

        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((BaseEntity)entityEntry.Entity).UpdatedDate = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).CreatedDate = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //Many-To-Many Relations

            modelBuilder.Entity<GroupStudent>().HasKey(gs => new { gs.GroupId, gs.StudentId});
            modelBuilder.Entity<StudentChord>().HasKey(sc => new { sc.StudentId, sc.ChordId });
            modelBuilder.Entity<StudentTabulature>().HasKey(st => new { st.StudentId, st.TabulatureId });

            modelBuilder.Entity<StudentChord>()
                .HasOne<StudentDAO>(s => s.Student)
                .WithMany(sc => sc.StudentChords)
                .HasForeignKey(s => s.StudentId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<StudentTabulature>()
                .HasOne<StudentDAO>(s => s.Student)
                .WithMany(sc => sc.StudentTabulatures)
                .HasForeignKey(s => s.StudentId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<GroupStudent>()
                .HasOne<StudentDAO>(s => s.Student)
                .WithMany(sc => sc.GroupStudents)
                .HasForeignKey(s => s.StudentId)
                .OnDelete(DeleteBehavior.NoAction);

            //One-To-Many Relations

            modelBuilder.Entity<TaskDAO>()
            .HasOne<TeacherDAO>(s => s.Teacher)
            .WithMany(g => g.Tasks)
            .HasForeignKey(s => s.TeacherId)
            .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<TaskDAO>()
            .HasOne<StudentDAO>(s => s.Student)
            .WithMany(g => g.Tasks)
            .HasForeignKey(s => s.StudentId);

            modelBuilder.Entity<TabulatureDAO>()
            .HasOne<TeacherDAO>(s => s.OwnerTeacher)
            .WithMany(g => g.CreatedTabs)
            .HasForeignKey(s => s.TeacherId);

            modelBuilder.Entity<ChordDAO>()
            .HasOne<TeacherDAO>(s => s.OwnerTeacher)
            .WithMany(g => g.CreatedChords)
            .HasForeignKey(s => s.TeacherId);

            modelBuilder.Entity<IndividualClassDAO>()
            .HasOne<TeacherDAO>(s => s.Teacher)
            .WithMany(g => g.IndividualClasses)
            .HasForeignKey(s => s.TeacherId)
            .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<GroupClassDAO>()
            .HasOne<TeacherDAO>(s => s.Teacher)
            .WithMany(g => g.GroupClasses)
            .HasForeignKey(s => s.TeacherId);

            modelBuilder.Entity<IndividualClassDAO>()
            .HasOne<StudentDAO>(s => s.Student)
            .WithMany(g => g.IndividualClasses)
            .HasForeignKey(s => s.StudentId);

            modelBuilder.Entity<DescriptiveEvaluationDAO>()
            .HasOne<StudentDAO>(s => s.Student)
            .WithMany(g => g.Evaluations)
            .HasForeignKey(s => s.StudentId);

            modelBuilder.Entity<DescriptiveEvaluationDAO>()
            .HasOne<TeacherDAO>(s => s.Teacher)
            .WithMany(g => g.DescriptiveEvaluations)
            .HasForeignKey(s => s.TeacherId)
            .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<AttendenceDAO>()
            .HasOne<GroupClassDAO>(s => s.GroupClass)
            .WithMany(g => g.Attendences)
            .HasForeignKey(s => s.GroupClassId)
            .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<UserDAO>()
            .HasOne<UserRoleDAO>(u => u.Role)
            .WithMany(r => r.Users)
            .HasForeignKey(u => u.RoleCode);

            //One-To-One Relations

            modelBuilder.Entity<GroupDAO>()
            .HasOne<GroupClassDAO>(s => s.Classes)
            .WithOne(ad => ad.Group)
            .HasForeignKey<GroupClassDAO>(ad => ad.GroupId);

            modelBuilder.Entity<PictureDAO>()
            .HasOne<ChordDAO>(s => s.Chord)
            .WithOne(ad => ad.Picture)
            .HasForeignKey<ChordDAO>(ad => ad.PictureId);
        }
    }
}
