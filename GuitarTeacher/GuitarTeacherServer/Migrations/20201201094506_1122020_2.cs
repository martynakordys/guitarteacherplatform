﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GuitarTeacherServer.Migrations
{
    public partial class _1122020_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isPublic",
                table: "Chords",
                newName: "IsPublic");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Chords",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Chords");

            migrationBuilder.RenameColumn(
                name: "IsPublic",
                table: "Chords",
                newName: "isPublic");
        }
    }
}
