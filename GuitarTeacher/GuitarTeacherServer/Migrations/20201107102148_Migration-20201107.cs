﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GuitarTeacherServer.Migrations
{
    public partial class Migration20201107 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Groups_GroupClasses_ClassesId",
                table: "Groups");

            migrationBuilder.DropIndex(
                name: "IX_Groups_ClassesId",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "ClassesId",
                table: "Groups");

            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "GroupClasses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_GroupClasses_GroupId",
                table: "GroupClasses",
                column: "GroupId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupClasses_Groups_GroupId",
                table: "GroupClasses",
                column: "GroupId",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupClasses_Groups_GroupId",
                table: "GroupClasses");

            migrationBuilder.DropIndex(
                name: "IX_GroupClasses_GroupId",
                table: "GroupClasses");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "GroupClasses");

            migrationBuilder.AddColumn<int>(
                name: "ClassesId",
                table: "Groups",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Groups_ClassesId",
                table: "Groups",
                column: "ClassesId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Groups_GroupClasses_ClassesId",
                table: "Groups",
                column: "ClassesId",
                principalTable: "GroupClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
