﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GuitarTeacherServer.Migrations
{
    public partial class _1122020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Content",
                table: "Chords");

            migrationBuilder.AddColumn<int>(
                name: "PictureId",
                table: "Chords",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "PictureDAO",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PictureData = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PictureDAO", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Chords_PictureId",
                table: "Chords",
                column: "PictureId");

            migrationBuilder.AddForeignKey(
                name: "FK_Chords_PictureDAO_PictureId",
                table: "Chords",
                column: "PictureId",
                principalTable: "PictureDAO",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chords_PictureDAO_PictureId",
                table: "Chords");

            migrationBuilder.DropTable(
                name: "PictureDAO");

            migrationBuilder.DropIndex(
                name: "IX_Chords_PictureId",
                table: "Chords");

            migrationBuilder.DropColumn(
                name: "PictureId",
                table: "Chords");

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Chords",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
