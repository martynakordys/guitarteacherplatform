﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GuitarTeacherServer.Migrations
{
    public partial class _1122020_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chords_PictureDAO_PictureId",
                table: "Chords");

            migrationBuilder.DropIndex(
                name: "IX_Chords_PictureId",
                table: "Chords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PictureDAO",
                table: "PictureDAO");

            migrationBuilder.RenameTable(
                name: "PictureDAO",
                newName: "Pictures");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pictures",
                table: "Pictures",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Chords_PictureId",
                table: "Chords",
                column: "PictureId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Chords_Pictures_PictureId",
                table: "Chords",
                column: "PictureId",
                principalTable: "Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chords_Pictures_PictureId",
                table: "Chords");

            migrationBuilder.DropIndex(
                name: "IX_Chords_PictureId",
                table: "Chords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Pictures",
                table: "Pictures");

            migrationBuilder.RenameTable(
                name: "Pictures",
                newName: "PictureDAO");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PictureDAO",
                table: "PictureDAO",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Chords_PictureId",
                table: "Chords",
                column: "PictureId");

            migrationBuilder.AddForeignKey(
                name: "FK_Chords_PictureDAO_PictureId",
                table: "Chords",
                column: "PictureId",
                principalTable: "PictureDAO",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
