﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GuitarTeacherServer.Migrations
{
    public partial class _16112020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "GivenDate",
                table: "Tasks",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GivenDate",
                table: "Tasks");
        }
    }
}
