﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IUserRoleService
    {
        UserRolesListDTO GetAvailalbeRoles();
        public string GetRoleNameByCode(string id);

    }
}
