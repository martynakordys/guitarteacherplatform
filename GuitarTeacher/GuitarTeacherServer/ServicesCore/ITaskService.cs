﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface ITaskService
    {
        void AddTask(TaskDTO taskToAdd, int teacherId, int studentId);
        TaskDTO GetTask(int id);
        TasksListDTO GetAllTasksForStudent(int studentId, bool realizedOnly);
        TasksListDTO GetAllTasksForTeacher(int teacherId, bool realizedOnly);
        void EditTask(TaskDTO taskToEdit);
        void DeleteTask(int id);
        void EvaluateTask(TaskDTO taskToEvaluate);
    }
}
