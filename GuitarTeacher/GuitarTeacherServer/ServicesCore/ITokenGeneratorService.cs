﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface ITokenGeneratorService
    {
        string GenerateToken(int userId);

    }
}
