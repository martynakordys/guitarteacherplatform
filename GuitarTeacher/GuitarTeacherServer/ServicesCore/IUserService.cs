﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IUserService
    {
        int GetUseIdrByLoginAndPassword(string login, string password);
        void AddUser(UserDTO newUserDTO);
        UserDTO GetUserById(int userId);
        bool IsTeacher(int userId);
        bool IsStudent(int userId);
        void UpdateUser(UserDTO userToUpdate);
        void UpdatePassword(LoginDTO newPassword, int userId);

    }
}
