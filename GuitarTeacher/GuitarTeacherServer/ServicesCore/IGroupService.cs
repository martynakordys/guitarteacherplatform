﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IGroupService
    {
        int CreateGroup(GroupClassDTO groupClass);

    }
}
