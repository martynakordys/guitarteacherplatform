﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IIndividualClassService
    {
        void AddIndividualClass(IndividualClassDTO classDTO, int teacherId, int studentId);
        IndividualClassDTO GetIndividualClass(int classId);
        IndividualClassesListDTO GetIndividualClassesForTeacher(int teacherId, bool unrealizedOnly);
        IndividualClassesListDTO GetIndividualClassesForStudent(int studentId, bool unrealizedOnly);
        void RealizeIndividualClass(int classId, string topic);
        void EditIndividualClass(IndividualClassDTO updatedClass);
        void DeleteIndividualClass(int id);
    }
}
