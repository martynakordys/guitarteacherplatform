﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IStudentService
    {
        int GetIdByUserLogin(string login);
        int GetStudentIdFromUserId(int userId);
        StudentDTO GetStudentById(int id);

    }
}
