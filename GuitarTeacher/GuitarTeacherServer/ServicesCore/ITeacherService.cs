﻿using GuitarTeacherServer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface ITeacherService 
    { 
        IEnumerable<TeacherDAO> GetTeachers();
        int GetTeacherIdFromUserId(int userId);
    }
}
