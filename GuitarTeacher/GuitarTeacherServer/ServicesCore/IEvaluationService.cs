﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IEvaluationService
    {
        EvaluationsListDTO GetAllEvaluationsForTeacher(int teacherId);
        EvaluationsListDTO GetAllEvaluationsForStudent(int studentId);
        EvaluationDTO GetEvaluation(int idToGet);
        void AddEvaluation(EvaluationDTO evaluationToAdd, int teacherId, int studentId);
        void EditEvaluation(EvaluationDTO evaluationToEdit);
        void DeleteEvaluation(int idToDelete);
    }
}
