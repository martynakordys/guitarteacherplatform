﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IChordService
    {
        Bitmap CreateChordImage(ChordSchemeDTO chordScheme);
        void AddChord(ChordDTO chordToAdd, Bitmap chordPicture, int teacherId);
        ChordsListDTO GetAllChordsForTeacher(int teacherId);
        ChordsListDTO GetAllChordsForStudent(int studentId);
        ChordDTO GetChord(int chordId);
        ChordsListDTO GetAllPublicChords();
        void DeleteChord(int chordId);
        void ShareChord(ShareObjectDTO shareObject);

    }
}
