﻿using GuitarTeacherServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.ServicesCore
{
    public interface IGroupClassService
    {
        void AddGroupClass(GroupClassDTO groupClass, int teacherId, int groupId);
        GroupClassDTO GetGroupClass(int classId);
        GroupClassesListDTO GetAllGroupClassesForTeacher(int teacherId, bool unrealizedOnly);
        GroupClassesListDTO GetAllGroupClassesForStudent(int studentId, bool unrealizedOnly);
        void RealizeGroupClass(AttendenceDTO attendence);
        void EditGroupClass(GroupClassDTO classToEdit);
        void DeleteGroupClass(int idToDelete);
        void UpdateAttendence(AttendenceDTO attendenceToUpdate);
    }
}
