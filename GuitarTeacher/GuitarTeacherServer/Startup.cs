using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuitarTeacherServer.Model;
using GuitarTeacherServer.Model.JoiningTables;
using GuitarTeacherServer.Repositories;
using GuitarTeacherServer.Repositories.RepositoryBase;
using GuitarTeacherServer.Services;
using GuitarTeacherServer.ServicesCore;
using GuitarTeacherServer.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace GuitarTeacherServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();

            //services.AddMvc(options =>
            //{
            //    options.Filters.Add(new AllowAnonymousFilter());
            //});

            TokenSettings tokenSettings = new TokenSettings();
            Configuration.GetSection("TokenSettings").Bind(tokenSettings);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            }).AddJwtBearer("JwtBearer", jwtOptions =>
            {
                jwtOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSettings.SecretKey)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromHours(5),
                };
            });

            //services
            services.AddTransient<ITeacherService, TeacherService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserRoleService, UserRoleService>();
            services.AddTransient<ITokenGeneratorService, TokenGeneratorService>();
            services.AddTransient<IIndividualClassService, IndividualClassService>();
            services.AddTransient<IUserProviderService, UserProviderService>();
            services.AddTransient<IStudentService, StudentService>();
            services.AddTransient<IGroupClassService, GroupClassService>();
            services.AddTransient<IGroupService, GroupService>();
            services.AddTransient<ITaskService, TaskService>();
            services.AddTransient<IEvaluationService, EvaluationService>();
            services.AddTransient<IChordService, ChordService>();




            services.AddHttpContextAccessor();
            services.AddSingleton<ITokenSettings>(tokenSettings);

            //DataBase
            services.AddDbContextPool<GuitarDBContext>(op => op.UseSqlServer("Server = (LocalDb)\\MSSQLLocalDB;Initial Catalog=GuitarDB;MultipleActiveResultSets=true;"));

            //repositories
            services.AddScoped<IRepository<TeacherDAO>, Repository<TeacherDAO>>();
            services.AddScoped<IRepository<IndividualClassDAO>, Repository<IndividualClassDAO>>();
            services.AddScoped<IRepository<UserDAO>, Repository<UserDAO>>();
            services.AddScoped<IRepository<StudentDAO>, Repository<StudentDAO>>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IRepository<ChordDAO>, Repository<ChordDAO>>();
            services.AddScoped<IRepository<DescriptiveEvaluationDAO>, Repository<DescriptiveEvaluationDAO>>();
            services.AddScoped<IRepository<GroupClassDAO>, Repository<GroupClassDAO>>();
            services.AddScoped<IRepository<GroupDAO>, Repository<GroupDAO>>();
            services.AddScoped<IRepository<TabulatureDAO>, Repository<TabulatureDAO>>();
            services.AddScoped<IRepository<TaskDAO>, Repository<TaskDAO>>();
            services.AddScoped<IRepository<AttendenceDAO>, Repository<AttendenceDAO>>();
            services.AddScoped<IRepository<GroupStudent>, Repository<GroupStudent>>();
            services.AddScoped<IRepository<PictureDAO>, Repository<PictureDAO>>();
            services.AddScoped<IRepository<StudentChord>, Repository<StudentChord>>();
            services.AddScoped<IRepository<StudentTabulature>, Repository<StudentTabulature>>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
