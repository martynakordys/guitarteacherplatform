﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [Route("groupClasses")]
    [ApiController]
    public class GroupClassController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IGroupClassService groupClassService;
        private readonly IGroupService groupService;
        private readonly ITeacherService teacherService;
        private readonly IStudentService studentService;

        public GroupClassController(IUserProviderService userProviderService,
                                    IUserService userService,
                                    IGroupClassService groupClassService,
                                    IGroupService groupService,
                                    ITeacherService teacherService,
                                    IStudentService studentService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.groupClassService = groupClassService;
            this.groupService = groupService;
            this.teacherService = teacherService;
            this.studentService = studentService;
        }

        [Authorize]
        [HttpPost("teacher")]
        public ActionResult AddGroupClass(GroupClassDTO groupClass)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    groupClassService.AddGroupClass(groupClass, teacherService.GetTeacherIdFromUserId(userId), groupService.CreateGroup(groupClass));
                    return Ok("Group class added successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpGet("{classId}")]
        public ActionResult GetGroupClassById(int classId)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    GroupClassDTO groupClass = groupClassService.GetGroupClass(classId);
                    return Ok(groupClass);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetAllUnrealizedIndividualClasses()
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    GroupClassesListDTO classListDTO = groupClassService.GetAllGroupClassesForTeacher(teacherService.GetTeacherIdFromUserId(userId), unrealizedOnly: true);
                    return Ok(classListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else if (userService.IsStudent(userId))
            {
                try
                {
                    GroupClassesListDTO classListDTO = groupClassService.GetAllGroupClassesForStudent(studentService.GetStudentIdFromUserId(userId), unrealizedOnly: true);
                    return Ok(classListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest();
        }

        [Authorize]
        [HttpGet("history")]
        public ActionResult GetAllRealizedIndividualClasses()
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    GroupClassesListDTO classListDTO = groupClassService.GetAllGroupClassesForTeacher(teacherService.GetTeacherIdFromUserId(userId), unrealizedOnly: false);
                    return Ok(classListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else if (userService.IsStudent(userId))
            {
                try
                {
                    GroupClassesListDTO classListDTO = groupClassService.GetAllGroupClassesForStudent(studentService.GetStudentIdFromUserId(userId), unrealizedOnly: false);
                    return Ok(classListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost("teacher/realize")]
        public ActionResult RealizeGroupClass(AttendenceDTO attendence)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    groupClassService.RealizeGroupClass(attendence);
                    return Ok("Group class realized successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpPatch("teacher")]
        public ActionResult EditGroupClass(GroupClassDTO updatedClass)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    groupClassService.EditGroupClass(updatedClass);
                    return Ok("Group class edited successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpPatch("teacher/attendence")]
        public ActionResult EditAttendence(AttendenceDTO attendenceToUpdate)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    groupClassService.UpdateAttendence(attendenceToUpdate);
                    return Ok("Attendence edited successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpDelete("teacher/{idToDelete}")]
        public ActionResult DeleteGroupClass(int idToDelete)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    groupClassService.DeleteGroupClass(idToDelete);
                    return Ok("Group class deleted successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }
    }
}

