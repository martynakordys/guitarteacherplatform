﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [Route("chords")]
    [ApiController]
    public class ChordController: ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IChordService chordService;
        private readonly ITeacherService teacherService;
        private readonly IStudentService studentService;

        public ChordController(IUserProviderService userProviderService,
                               IUserService userService,
                               IChordService chordService,
                               ITeacherService teacherService,
                               IStudentService studentService) 
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.chordService = chordService;
            this.teacherService = teacherService;
            this.studentService = studentService;
        }

        [Authorize]
        [HttpPost("teacher")]
        public ActionResult AddChord(ChordDTO chordToAdd)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    chordService.AddChord(chordToAdd, chordService.CreateChordImage(chordToAdd.Scheme), teacherService.GetTeacherIdFromUserId(userId));
                    return Ok("Chord added succesfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpGet()]
        public ActionResult GetAllChords()
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                var chordsList = chordService.GetAllChordsForTeacher(teacherService.GetTeacherIdFromUserId(userId));
                return Ok(chordsList);
            }
            else if (userService.IsStudent(userId))
            {
                var chordsList = chordService.GetAllChordsForStudent(studentService.GetStudentIdFromUserId(userId));
                return Ok(chordsList);
            }
            else
            {
                return Unauthorized("Invalid token");
            }
        }

        [Authorize]
        [HttpGet("public")]
        public ActionResult GetAllPublicChords()
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId)|| userService.IsStudent(userId))
            {
                var chordsList = chordService.GetAllPublicChords();
                return Ok(chordsList);
            }
            else
            {
                return Unauthorized("Invalid token");
            }
        }

        [Authorize]
        [HttpGet("{chordId}")]
        public ActionResult GetChord(int chordId)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId) || userService.IsStudent(userId))
            {
                var chord = chordService.GetChord(chordId);
                return Ok(chord);
            }
            else
            {
                return Unauthorized("Invalid token");
            }
        }

        [Authorize]
        [HttpPost("teacher/share")]
        public ActionResult ShareChordWithStudent(ShareObjectDTO chordToShare)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    chordService.ShareChord(chordToShare);
                    return Ok("Chord shared successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpDelete("teacher/{chordId}")]
        public ActionResult DeleteChord(int chordId)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                chordService.DeleteChord(chordId);
                return Ok("Chord deleted successfully");
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

    }
}
