﻿using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [Route("students")]
    [ApiController]
    public class StudentController: ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IStudentService studentService;

        public StudentController(IUserProviderService userProviderService,
                                 IUserService userService,
                                 IStudentService studentService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.studentService = studentService;
        }

        [Authorize]
        [HttpGet("{login}")]
        public ActionResult GetStudentByLogin(string login)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    var studentId = studentService.GetIdByUserLogin(login);
                    return Ok(studentService.GetStudentById(studentId));
                }
                catch(Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }
    }
}
