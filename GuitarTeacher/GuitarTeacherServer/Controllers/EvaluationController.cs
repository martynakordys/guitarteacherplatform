﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [Route("evaluations")]
    [ApiController]
    public class EvaluationController: ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IEvaluationService evaluationService;
        private readonly ITeacherService teacherService;
        private readonly IStudentService studentService;

        public EvaluationController(IUserProviderService userProviderService,
                                    IUserService userService,
                                    IEvaluationService evaluationService,
                                    ITeacherService teacherService,
                                    IStudentService studentService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.evaluationService = evaluationService;
            this.teacherService = teacherService;
            this.studentService = studentService;
        }

        [Authorize]
        [HttpPost("teacher")]
        public ActionResult AddEvaluation(EvaluationDTO evaluationToAdd)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    evaluationService.AddEvaluation(evaluationToAdd, teacherService.GetTeacherIdFromUserId(userId), studentService.GetIdByUserLogin(evaluationToAdd.StudentLogin));
                    return Ok("Evaluation added succesfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpGet()]
        public ActionResult GetAllEvaluations()
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    var evaluationsList = evaluationService.GetAllEvaluationsForTeacher(teacherService.GetTeacherIdFromUserId(userId));
                    return Ok(evaluationsList);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else if (userService.IsStudent(userId))
            {
                try
                {
                    var evaluationsList = evaluationService.GetAllEvaluationsForStudent(studentService.GetStudentIdFromUserId(userId));
                    return Ok(evaluationsList);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher or student!");
            }
        }

        [Authorize]
        [HttpGet("{evaluationId}")]
        public ActionResult GetEvaluation(int evaluationId)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    var evaluation = evaluationService.GetEvaluation(evaluationId);
                    return Ok(evaluation);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPatch("teacher")]
        public ActionResult EditEvaluation(EvaluationDTO evaluationToEdit)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    evaluationService.EditEvaluation(evaluationToEdit);
                    return Ok("Evaluation edited successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpDelete("teacher/{evaluationId}")]
        public ActionResult DeleteEvaluation(int evaluationId)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    evaluationService.DeleteEvaluation(evaluationId);
                    return Ok("Evaluation deleted successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }
    }
}
