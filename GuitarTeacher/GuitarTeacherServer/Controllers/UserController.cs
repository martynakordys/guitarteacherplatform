﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.Services;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IUserRoleService userRoleService;
        private readonly ITokenGeneratorService tokenGeneratorService;
        private readonly IUserProviderService userProviderService;

        public UserController(IUserService userService, 
                              IUserRoleService userRoleService, 
                              ITokenGeneratorService tokenGeneratorService,
                              IUserProviderService userProviderService)
        {
            this.userService = userService;
            this.userRoleService = userRoleService;
            this.tokenGeneratorService = tokenGeneratorService;
            this.userProviderService = userProviderService;
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginDTO loginDTO)
        {
            if (loginDTO == null || string.IsNullOrWhiteSpace(loginDTO.Login) || string.IsNullOrEmpty(loginDTO.Password))
            {
                return BadRequest("Invalid DTO");
            }
            try
            {
                int userId = userService.GetUseIdrByLoginAndPassword(loginDTO.Login, loginDTO.Password);
                string token = tokenGeneratorService.GenerateToken(userId);
                var user = userService.GetUserById(userId);
                var userRoleName = userRoleService.GetRoleNameByCode(user.RoleCode);

                UserDTO userDTO = new UserDTO
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Login = user.Login,
                    RoleName = userRoleName,
                    RoleCode = user.RoleCode,
                    Token = token
                };

                return Ok(userDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        public IActionResult AddUser([FromBody] UserDTO userDTO)
        {
                try
                {
                    userService.AddUser(userDTO);
                    return Ok("User added successfully");
                }
                catch (Exception exception)
                {
                    return BadRequest(exception.Message);
                }
        }

        [Authorize]
        [HttpGet()]
        public IActionResult GetUser()
        {
            var userId = userProviderService.GetUserId();

            try
            {
                var userDTO = userService.GetUserById(userId);
                return Ok(userDTO);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        [Authorize]
        [HttpPatch()]
        public IActionResult UpdateUser(UserDTO userToUpdate)
        {
            try
            {
                userService.UpdateUser(userToUpdate);
                return Ok("Data updated successfully");
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        [Authorize]
        [HttpPatch("password")]
        public IActionResult UpdatePassword(LoginDTO newPassword)
        {
            var userId = userProviderService.GetUserId();
            try
            {
                userService.UpdatePassword(newPassword, userId);
                return Ok("Password updated successfully");
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

    }
}
