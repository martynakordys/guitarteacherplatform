﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [Route("tasks")]
    [ApiController]
    public class TaskController: ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly ITaskService taskService;
        private readonly IStudentService studentService;
        private readonly ITeacherService teacherService;

        public TaskController(IUserProviderService userProviderService,
                              IUserService userService,
                              ITaskService taskService,
                              IStudentService studentService,
                              ITeacherService teacherService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.taskService = taskService;
            this.studentService = studentService;
            this.teacherService = teacherService;
        }

        [Authorize]
        [HttpPost("teacher")]
        public ActionResult AddTask(TaskDTO taskToAdd)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    taskService.AddTask(taskToAdd, teacherService.GetTeacherIdFromUserId(userId), studentService.GetIdByUserLogin(taskToAdd.StudentLogin));
                    return Ok("Task added successfully");
                }
                catch(Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpGet()]
        public ActionResult GetUnrealizedTasks()
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                var tasks = taskService.GetAllTasksForTeacher(teacherService.GetTeacherIdFromUserId(userId), realizedOnly: false);
                return Ok(tasks);
            }
            else if (userService.IsStudent(userId))
            {
                var tasks = taskService.GetAllTasksForStudent(studentService.GetStudentIdFromUserId(userId), realizedOnly: false);
                return Ok(tasks);
            }
            else
            {
                return Unauthorized("Invalid token");
            }
        }

        [Authorize]
        [HttpGet("history")]
        public ActionResult GetRealizedTasks()
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                var tasks = taskService.GetAllTasksForTeacher(teacherService.GetTeacherIdFromUserId(userId), realizedOnly: true);
                return Ok(tasks);
            }
            else if (userService.IsStudent(userId))
            {
                var tasks = taskService.GetAllTasksForStudent(studentService.GetStudentIdFromUserId(userId), realizedOnly: true);
                return Ok(tasks);
            }
            else
            {
                return Unauthorized("Invalid token");
            }
        }

        [Authorize]
        [HttpGet("{taskId}")]
        public ActionResult GetTask(int taskId)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    var task = taskService.GetTask(taskId);
                    return Ok(task);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpPatch("teacher")]
        public ActionResult EditTask(TaskDTO taskToEdit)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    taskService.EditTask(taskToEdit);
                    return Ok("Task edited successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpDelete("teacher/{taskId}")]
        public ActionResult DeleteTask(int taskId)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    taskService.DeleteTask(taskId);
                    return Ok("Task deleted successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

        [Authorize]
        [HttpPost("teacher/evaluate")]
        public ActionResult EvaluateTask(TaskDTO taskToEvaluate)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    taskService.EvaluateTask(taskToEvaluate);
                    return Ok("Task evaluated successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }

    }
}
