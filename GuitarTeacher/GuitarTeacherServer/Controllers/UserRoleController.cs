﻿using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [ApiController]
    [Route("userRoles")]
    public class UserRoleController: ControllerBase
    {
        private readonly IUserRoleService userRoleService;

        public UserRoleController(IUserRoleService userRoleService)
        {
            this.userRoleService = userRoleService;
        }
        [HttpGet()]
        public IActionResult GetAvailableRoles()
        {
            try
            {
                var userRolesList = userRoleService.GetAvailalbeRoles();
                return Ok(userRolesList);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
