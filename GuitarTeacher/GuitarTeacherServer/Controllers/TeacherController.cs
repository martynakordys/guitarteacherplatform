﻿using GuitarTeacherServer.Model;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [Route("api/teachers")]
    [ApiController]
    public class TeacherController: ControllerBase
    {
        private readonly ITeacherService teacherService;
        public TeacherController(ITeacherService teacherService)
        {
            this.teacherService = teacherService;
        }
    }
}
