﻿using GuitarTeacherServer.DTO;
using GuitarTeacherServer.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Controllers
{
    [Route("individualClasses")]
    [ApiController]
    public class IndividualClassController : ControllerBase
    {
        private readonly IIndividualClassService individualClassService;
        private readonly ITeacherService teachersService;
        private readonly IStudentService studentService;
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;

        public IndividualClassController(IIndividualClassService individualClassService,
            ITeacherService teachersService,
            IStudentService studentService,
            IUserProviderService userProviderService,
            IUserService userService)
        {
            this.individualClassService = individualClassService;
            this.teachersService = teachersService;
            this.studentService = studentService;
            this.userProviderService = userProviderService;
            this.userService = userService;
        }

        [Authorize]
        [HttpPost("teacher")]
        public ActionResult AddIndividualClass(IndividualClassDTO individualClassDTO)
        {
            var userId = userProviderService.GetUserId();
            if (userService.IsTeacher(userId))
            {
                try
                {
                    individualClassService.AddIndividualClass(individualClassDTO, teachersService.GetTeacherIdFromUserId(userId), studentService.GetIdByUserLogin(individualClassDTO.StudentLogin));
                    return Ok("Individual class added successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }

        }

        [Authorize]
        [HttpGet("{classId}")]
        public ActionResult GetIndividualClassById(int classId)
        {
            try
            {
                IndividualClassDTO individualClass = individualClassService.GetIndividualClass(classId);
                return Ok(individualClass);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [Authorize]
        [HttpGet]
        public ActionResult GetAllUnrealizedIndividualClasses()
        {
            var userId = userProviderService.GetUserId();

            if(userService.IsTeacher(userId))
            {
                try
                {
                    IndividualClassesListDTO classListDTO = individualClassService.GetIndividualClassesForTeacher(teachersService.GetTeacherIdFromUserId(userId), unrealizedOnly: true);
                    return Ok(classListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            } 

            else if (userService.IsStudent(userId))
            {
                try
                {
                    IndividualClassesListDTO classesListDTO = individualClassService.GetIndividualClassesForStudent(studentService.GetStudentIdFromUserId(userId), unrealizedOnly: true);
                    return Ok(classesListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            else
            {
                return Unauthorized("Invalid token");
            }
            

        }

        [Authorize]
        [HttpGet("history")]
        public ActionResult GetAllRealizedIndividualClasses()
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    IndividualClassesListDTO classListDTO = individualClassService.GetIndividualClassesForTeacher(teachersService.GetTeacherIdFromUserId(userId), unrealizedOnly: false);
                    return Ok(classListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            else if (userService.IsStudent(userId))
            {
                try
                {
                    IndividualClassesListDTO classesListDTO = individualClassService.GetIndividualClassesForStudent(studentService.GetStudentIdFromUserId(userId), unrealizedOnly: false);
                    return Ok(classesListDTO);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            else
            {
                return Unauthorized("Invalid token");
            }

        }

        [Authorize]
        [HttpPatch("teacher/realize")]
        public ActionResult RealizeLesson(IndividualClassDTO classToUpdate)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    individualClassService.RealizeIndividualClass(classToUpdate.Id, classToUpdate.Topic);
                    return Ok("Class realized successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }

        }

        [Authorize]
        [HttpPatch("teacher")]
        public ActionResult EditIndividualClass(IndividualClassDTO updatedClass)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    individualClassService.EditIndividualClass(updatedClass);
                    return Ok("Class updated successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }

        }

        [Authorize]
        [HttpDelete("teacher/{idToDelete}")]
        public ActionResult DeleteIndividualClass(int idToDelete)
        {
            var userId = userProviderService.GetUserId();

            if (userService.IsTeacher(userId))
            {
                try
                {
                    individualClassService.DeleteIndividualClass(idToDelete);
                    return Ok("Class deleted successfully");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return Unauthorized("Permission denied: you must be a teacher!");
            }
        }
    }
}
