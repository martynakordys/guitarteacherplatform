﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class IndividualClassesListDTO
    {
        public List<IndividualClassDTO> IndividualClasses { get; set; }
        public IndividualClassesListDTO()
        {
            IndividualClasses = new List<IndividualClassDTO>();
        }
    }
}
