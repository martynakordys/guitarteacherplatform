﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class ChordsListDTO
    {
        public List<ChordDTO> Chords { get; set; }

        public ChordsListDTO()
        {
            Chords = new List<ChordDTO>();
        }

    }
}
