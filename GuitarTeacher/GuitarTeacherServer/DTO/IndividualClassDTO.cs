﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class IndividualClassDTO
    {
        public int Id { get; set; }
        public DateTime DateOfClasses { get; set; }
        public int DurationTime { get; set; }

        public string StudentLogin { get; set; }

        public string StudentName { get; set; }

        public string TeacherName { get; set; }

        public string Topic { get; set; }
    }
}
