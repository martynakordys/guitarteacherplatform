﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class GroupClassesListDTO
    {
        public List<GroupClassDTO> GroupClasses { get; set; }
        public GroupClassesListDTO()
        {
            GroupClasses = new List<GroupClassDTO>();
        }
    }
}
