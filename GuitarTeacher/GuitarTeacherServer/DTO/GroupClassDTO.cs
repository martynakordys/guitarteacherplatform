﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class GroupClassDTO
    {
        public int Id { get; set; }
        public DateTime DateOfClass { get; set; }
        public int Duration { get; set; }
        public string Topic { get; set; }
        public List<StudentDTO> Students { get; set; }
        public string TeacherName { get; set; }

        public GroupClassDTO()
        {
            Students = new List<StudentDTO>();
        }
    }
}
