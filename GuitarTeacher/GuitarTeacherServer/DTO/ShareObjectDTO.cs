﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class ShareObjectDTO
    {
        public int ObjectId { get; set; }
        public List<int> StudentsIds { get; set; }

        public ShareObjectDTO()
        {
            StudentsIds = new List<int>();
        }
    }
}
