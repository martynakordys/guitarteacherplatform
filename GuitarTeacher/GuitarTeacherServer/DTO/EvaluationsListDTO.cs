﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class EvaluationsListDTO
    {
        public List<EvaluationDTO> Evaluations { get; set; }

        public EvaluationsListDTO()
        {
            Evaluations = new List<EvaluationDTO>();
        }
    }
}
