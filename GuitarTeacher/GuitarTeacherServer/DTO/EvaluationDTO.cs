﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class EvaluationDTO
    {
        public int Id { get; set; }
        public string EvaluationContent { get; set; }
        public DateTime GivenDate { get; set; }
        public string StudentLogin { get; set; }
        public string StudentName { get; set; }
        public string TeacherName { get; set; }
    }
}
