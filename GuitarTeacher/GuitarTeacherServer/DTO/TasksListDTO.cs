﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class TasksListDTO
    {
        public List<TaskDTO> Tasks { get; set; }
        public TasksListDTO()
        {
            Tasks = new List<TaskDTO>();
        }
    }
}
