﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class ChordDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsPublic { get; set; }
        public ChordSchemeDTO Scheme { get; set; }
        public DateTime UploadDate { get; set; }
        public byte[] ChordImage { get; set; }
        public string TeacherName { get; set; }
        public List<StudentDTO> SharingStudents { get; set; }

        public ChordDTO()
        {
            SharingStudents = new List<StudentDTO>();
        }
    }
}
