﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class UserRolesListDTO
    {
        public List<UserRoleDTO> UserRoles { get; set; }
        public UserRolesListDTO()
        {
            UserRoles = new List<UserRoleDTO>();
        }
    }
}
