﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.DTO
{
    public class AttendenceDTO
    {
        public int GroupClassId { get; set; }
        public string Topic { get; set; }
        public List<int> PresentStudentsIds { get; set; }
        public List<int> AbsentStudentsIds { get; set; }

        public AttendenceDTO()
        {
            PresentStudentsIds = new List<int>();
            AbsentStudentsIds = new List<int>();
        }
    }
}
