﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("UserRole", Schema = "dbo")]
    public class UserRoleDAO
    {
        [Required]
        [Key]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        public List<UserDAO> Users { get; set; }
    }
}
