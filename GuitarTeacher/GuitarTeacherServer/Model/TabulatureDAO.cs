﻿using GuitarTeacherServer.Model.Base;
using GuitarTeacherServer.Model.JoiningTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("Tabulatures")]
    public class TabulatureDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public int TeacherId { get; set; }

        [Required]
        public bool IsPublic { get; set; }

        public TeacherDAO OwnerTeacher { get; set; }

        public List<StudentTabulature> StudentTabulatures { get; set; }

    }
}
