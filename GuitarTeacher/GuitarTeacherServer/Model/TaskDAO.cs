﻿using GuitarTeacherServer.Model.Base;
using GuitarTeacherServer.Model.JoiningTables;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("Tasks")]
    public class TaskDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string TaskContent { get; set; }

        [Required]
        public DateTime GivenDate { get; set; }
        
        [Required]
        public bool IsRealized { get; set; }

        public int Note { get; set; }

        [Required]
        public int TeacherId { get; set; }
        public TeacherDAO Teacher { get; set; }

        [Required]
        public int StudentId { get; set; }
        public StudentDAO Student { get; set; }

    }
}
