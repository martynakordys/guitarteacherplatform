﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model.JoiningTables
{
    public class StudentTabulature
    {
        public int StudentId { get; set; }
        public StudentDAO Student { get; set; }
        public int TabulatureId { get; set; }
        public TabulatureDAO Tabulature { get; set; }
    }
}
