﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model.JoiningTables
{
    public class StudentChord
    {
        public int StudentId { get; set; }
        public StudentDAO Student { get; set; }

        public int ChordId { get; set; }
        public ChordDAO Chord { get; set; }
    }
}
