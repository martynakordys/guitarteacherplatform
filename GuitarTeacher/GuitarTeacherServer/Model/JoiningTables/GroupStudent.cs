﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model.JoiningTables
{
    public class GroupStudent
    {
        public int StudentId { get; set; }
        public StudentDAO Student { get; set; }
        public int GroupId { get; set; }
        public GroupDAO Group { get; set; }
    }
}
