﻿using GuitarTeacherServer.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    public class AttendenceDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public bool IsPresent { get; set; }

        [Required]
        public int GroupClassId { get; set; }

        public GroupClassDAO GroupClass { get; set; }

        [Required]
        public int StudentId { get; set; }

        public StudentDAO Student { get; set; }
    }
}
