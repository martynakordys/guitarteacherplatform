﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    public class PictureDAO
    {
        public int Id { get; set; }
        public byte[] PictureData { get; set; }
        public ChordDAO Chord { get; set; }
    }
}
