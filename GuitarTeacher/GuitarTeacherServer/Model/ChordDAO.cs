﻿using GuitarTeacherServer.Model.Base;
using GuitarTeacherServer.Model.JoiningTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("Chords")]
    public class ChordDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public int PictureId { get; set; }
        public PictureDAO Picture { get; set; }

        public DateTime UploadDate { get; set; }

        [Required]
        public bool IsPublic { get; set; }

        public string Name { get; set; }

        [Required]
        public int TeacherId { get; set; }

        public TeacherDAO OwnerTeacher { get; set; }

        public List<StudentChord> StudentChords { get; set; }

    }
}
