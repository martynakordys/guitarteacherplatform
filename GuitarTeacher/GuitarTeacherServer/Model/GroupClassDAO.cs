﻿using GuitarTeacherServer.Model.Base;
using GuitarTeacherServer.Model.JoiningTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("GroupClasses")]
    public class GroupClassDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public DateTime DateOfClasses { get; set; }

        [Required]
        public int DurationMin { get; set; }

        public string Topic { get; set; }

        [Required]
        public bool IsRealized { get; set; }

        [Required]
        public int TeacherId { get; set; }

        public TeacherDAO Teacher { get; set; }
        public GroupDAO Group { get; set; }
        public int GroupId { get; set; }
        public List<AttendenceDAO> Attendences { get; set; }

    }
}
