﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("Teachers")]
    public class TeacherDAO
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public int UserId { get; set; }

        public UserDAO User { get; set; }

        public List<IndividualClassDAO> IndividualClasses { get; set; }
        public List<GroupClassDAO> GroupClasses { get; set; }
        public List<TabulatureDAO> CreatedTabs { get; set; }
        public List<ChordDAO> CreatedChords { get; set; }
        public List<TaskDAO> Tasks { get; set; }
        public List<DescriptiveEvaluationDAO> DescriptiveEvaluations { get; set; }
    }
}
