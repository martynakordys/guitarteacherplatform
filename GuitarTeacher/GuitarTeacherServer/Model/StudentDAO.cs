﻿using GuitarTeacherServer.Model.JoiningTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("Students")]
    public class StudentDAO
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public int UserId { get; set; }
        public UserDAO User { get; set; }

        public List<IndividualClassDAO> IndividualClasses { get; set; }
        public List<DescriptiveEvaluationDAO> Evaluations { get; set; }
        public List<GroupStudent> GroupStudents { get; set; }
        public List<StudentChord> StudentChords { get; set; }
        public List<StudentTabulature> StudentTabulatures { get; set; }
        public List<TaskDAO> Tasks { get; set; }
    }
}
