﻿using GuitarTeacherServer.Model.Base;
using GuitarTeacherServer.Model.JoiningTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("IndividualClasses")]
    public class IndividualClassDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public DateTime DateOfClasses { get; set; }

        [Required]
        public int DurationMin { get; set; }

        [Required]
        public bool IsRealized { get; set; }

        public string Topic { get; set; }

        [Required]
        public int TeacherId { get; set; }

        public TeacherDAO Teacher { get; set; }

        public int StudentId { get; set; }
        public StudentDAO Student { get; set; }
    }
}
