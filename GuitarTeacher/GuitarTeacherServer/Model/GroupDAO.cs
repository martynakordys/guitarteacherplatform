﻿using GuitarTeacherServer.Model.Base;
using GuitarTeacherServer.Model.JoiningTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("Groups")]
    public class GroupDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }     
        public GroupClassDAO Classes { get; set; }
        public List<GroupStudent> GroupStudents { get; set; }

    }
}
