﻿using GuitarTeacherServer.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    [Table("User", Schema = "dbo")]
    public class UserDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public string RoleCode { get; set; }

        public UserRoleDAO Role { get; set; }
    }
}
