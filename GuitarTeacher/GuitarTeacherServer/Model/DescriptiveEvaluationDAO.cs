﻿using GuitarTeacherServer.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Model
{
    public class DescriptiveEvaluationDAO: BaseEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string EvaluationContent { get; set; }

        [Required]
        public DateTime GivenDate { get; set; }

        [Required]
        public int StudentId { get; set; }
        public StudentDAO Student { get; set; }

        [Required]
        public int TeacherId { get; set; }
        public TeacherDAO Teacher { get; set; }
    }
}
