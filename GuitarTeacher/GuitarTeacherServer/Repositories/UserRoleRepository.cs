﻿using GuitarTeacherServer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Repositories
{
    public class UserRoleRepository: IUserRoleRepository
    {
        private readonly GuitarDBContext context;

        public UserRoleRepository(GuitarDBContext context)
        {
            this.context = context;
        }

        public UserRoleDAO Get(string Id)
        {
            return this.context.Set<UserRoleDAO>().Find(Id);
        }

        public IEnumerable<UserRoleDAO> GetAll()
        {
            return this.context.Set<UserRoleDAO>();
        }
    }
}
