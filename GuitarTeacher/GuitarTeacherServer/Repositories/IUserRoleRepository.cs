﻿using GuitarTeacherServer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Repositories
{
    public interface IUserRoleRepository
    {
        UserRoleDAO Get(string Id);
        IEnumerable<UserRoleDAO> GetAll();
    }
}
