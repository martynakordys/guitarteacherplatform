﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Repositories.RepositoryBase
{
    public interface IRepository<TEntity> where TEntity: class
    {
        TEntity Get(int Id);
        IEnumerable<TEntity> GetAll();
        TEntity Delete(int Id);
        TEntity DeleteEntity(TEntity toDelete);
        TEntity Add(TEntity tObject);
        TEntity Update(TEntity tObject);
    }
}
