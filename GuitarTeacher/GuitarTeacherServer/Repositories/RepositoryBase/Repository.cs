﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherServer.Repositories.RepositoryBase
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly GuitarDBContext context;

        public Repository(GuitarDBContext context)
        {
            this.context = context;
        }

        public TEntity Add(TEntity tObject)
        {
            this.context.Set<TEntity>().Add(tObject);
            this.context.SaveChanges();
            return tObject;
        }

        public TEntity Delete(int Id)
        {
            TEntity obj = this.context.Set<TEntity>().Find(Id);
            if (obj!=null)
            {
                this.context.Set<TEntity>().Remove(obj);
                this.context.SaveChanges();
            }
            return obj;
        }

        public TEntity DeleteEntity(TEntity toDelete)
        {
            if (toDelete != null)
            {
                this.context.Set<TEntity>().Remove(toDelete);
                this.context.SaveChanges();
            }
            return toDelete;
        }

        public TEntity Get(int Id)
        {
            return this.context.Set<TEntity>().Find(Id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return this.context.Set<TEntity>();
        }

        public TEntity Update(TEntity tObject)
        {
            this.context.Entry(tObject);
            var obj = this.context.Set<TEntity>().Attach(tObject);
            obj.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            this.context.SaveChanges();
            return tObject;
        }
    }
}
