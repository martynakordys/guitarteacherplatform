﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class Message
    {
        public bool isOK { get; set; }
        public string MessageContent { get; set; }
    }
}
