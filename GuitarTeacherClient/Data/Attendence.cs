﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class Attendence
    {
        public int GroupClassId { get; set; }
        public string Topic { get; set; }
        public List<int> PresentStudentsIds { get; set; }
        public List<int> AbsentStudentsIds { get; set; }

        public Attendence()
        {
            PresentStudentsIds = new List<int>();

            AbsentStudentsIds = new List<int>();
        }
    }
}
