﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class ChordsList
    {
        public List<Chord> Chords { get; set; }

        public ChordsList()
        {
            Chords = new List<Chord>();
        }

    }
}
