﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class IndividualClass
    {
        public int Id { get; set; }
        public DateTime DateOfClasses { get; set; }

        [RegularExpression("\\d{2,3}", ErrorMessage ="Invalid value")]
        public string DurationTimeString { get; set; }
        public int DurationTime { get; set; }
        public string Topic { get; set; }
        public string StudentLogin { get; set; }
        public string StudentName { get; set; }
        public string TeacherName { get; set; }
    }
}
