﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class Student
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public bool IsPresent { get; set; }
    }
}
