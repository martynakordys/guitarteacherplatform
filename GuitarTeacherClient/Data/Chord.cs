﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class Chord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsPublic { get; set; }
        public ChordScheme Scheme { get; set; }
        public DateTime UploadDate { get; set; }
        public byte[] ChordImage { get; set; }
        public string TeacherName { get; set; }
        public List<Student> SharingStudents { get; set; }

        public Chord()
        {
            SharingStudents = new List<Student>();
        }
    }
}
