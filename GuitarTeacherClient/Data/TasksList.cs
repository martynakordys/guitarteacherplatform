﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class TasksList
    {
        public List<TaskObj> Tasks { get; set; }

        public TasksList()
        {
            Tasks = new List<TaskObj>();
        }
    }
}
