﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class GroupClass
    {
        public int Id { get; set; }
        public DateTime DateOfClass { get; set; }
        public int Duration { get; set; }

        [RegularExpression("\\d{2,3}", ErrorMessage = "Invalid value")]
        public string DurationString { get; set; }
        public string Topic { get; set; }
        public List<Student> Students { get; set; }
        public string TeacherName { get; set; }

        public GroupClass()
        {
            Students = new List<Student>();
        }
    }
    
}
