﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GuitarTeacherClient.Data
{
    public class LoginObject
    {
        [Required(ErrorMessage ="Podaj login użytkownika")]
        public string Login { get; set; }

        [Required(ErrorMessage ="Podaj haslo")]
        public string Password { get; set; }

        public string NewPassword { get; set; }
    }
}
