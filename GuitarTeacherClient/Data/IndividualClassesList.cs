﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class IndividualClassesList
    {
        public List<IndividualClass> IndividualClasses { get; set; }

        public IndividualClassesList()
        {
            IndividualClasses = new List<IndividualClass>();
        }
    }
}
