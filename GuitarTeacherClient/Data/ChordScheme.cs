﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class ChordScheme
    {
        public char[] StringsSymbols { get; set; }
        public char[][] FingersSymbols { get; set; }
        public int FretNumber { get; set; }

        public ChordScheme()
        {
            StringsSymbols = new char[6];
            FingersSymbols = new char[5][];
            for (int i = 0; i < 5; i++)
            {
                FingersSymbols[i] = new char[6];
            }
        }
    }
}
