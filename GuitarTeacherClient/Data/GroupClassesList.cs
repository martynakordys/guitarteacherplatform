﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class GroupClassesList
    {
        public List<GroupClass> GroupClasses { get; set; }

        public GroupClassesList()
        {
            GroupClasses = new List<GroupClass>();
        }
    }
}
