﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class ShareObject
    {
        public int ObjectId { get; set; }
        public List<int> StudentsIds { get; set; }

        public ShareObject()
        {
            StudentsIds = new List<int>();
        }
    }
}
