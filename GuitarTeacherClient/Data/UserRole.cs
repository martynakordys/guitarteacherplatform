﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class UserRole
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
