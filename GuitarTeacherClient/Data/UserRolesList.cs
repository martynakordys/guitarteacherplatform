﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class UserRolesList
    {
        public List<UserRole> UserRoles { get; set; }

        public UserRolesList()
        {
            UserRoles = new List<UserRole>();
        }
    }
}
