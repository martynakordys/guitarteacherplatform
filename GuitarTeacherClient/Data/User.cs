﻿using GuitarTeacherClient.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        [Required(ErrorMessage ="Confirm password")]
        [CompareProperty(otherProperty:"Password", ErrorMessage = "Password isn't identical")]
        public string PasswordConfirmation { get; set; }

        [Required(ErrorMessage ="Firstname is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Lastname is required")]
        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        [NonIndex(ErrorMessage ="Choose user type")]
        public string RoleCode { get; set; }

        public string RoleName { get; set; }

        public string Token { get; set; }

        public string ErrorMessage { get; set; }
    }
}
