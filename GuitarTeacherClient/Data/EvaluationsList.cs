﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Data
{
    public class EvaluationsList
    {
        public List<Evaluation> Evaluations { get; set; }

        public EvaluationsList()
        {
            Evaluations = new List<Evaluation>();
        }
    }
}
