﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    public interface IIndividualClassService
    {
        Task<string> AddClasses(IndividualClass classObject);
        Task<IndividualClassesList> GetUnrealizedClasses();
        Task<IndividualClassesList> GetRealizedClass();
        Task<string> EditClasses(IndividualClass classObject);
        Task<string> DeleteClasses(int id);
        Task<string> RealizeClasses(IndividualClass classObject);
    }
}
