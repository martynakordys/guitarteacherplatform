﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    public interface ITaskService
    {
        Task<string> AddClass(TaskObj toAdd);
        Task<TasksList> GetRelizedTasks();
        Task<TasksList> GetUnrealizedTasks();
        Task<string> EditTaskContent(TaskObj toEdit);
        Task<string> EvaluateTask(TaskObj toEvaluate);
        Task<string> DeleteTask(int idToDelete);
    }
}
