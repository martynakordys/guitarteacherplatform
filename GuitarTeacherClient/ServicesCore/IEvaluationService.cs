﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    public interface IEvaluationService
    {
        Task<EvaluationsList> GetEvaluations();
        Task<string> AddEvaluation(Evaluation evaluationToAdd);
        Task<string> EditEvaluation(Evaluation evaluationToEdit);
        Task<string> DeleteEvaluation(int idToDelete);
    }
}
