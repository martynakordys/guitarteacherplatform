﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    public interface IStudentService
    {
        Task<Student> GetStudentByLogin(string login);

    }
}
