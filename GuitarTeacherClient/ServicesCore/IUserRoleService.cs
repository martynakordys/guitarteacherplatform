﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    public interface IUserRoleService
    {
        Task<UserRolesList> GetAvailableRoles();
    }
}
