﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    public interface IGroupClassService
    {
        Task<string> AddClasses(GroupClass classObject);
        Task<GroupClassesList> GetUnrealizedClasses();
        Task<GroupClassesList> GetRealizedClasses();
        Task<GroupClass> GetClass(int id);
        Task<string> RealizeClass(Attendence attendence);
        Task<string> EditClass(GroupClass groupClass);
        Task<string> DeleteClass(int id);
        Task<string> EditAttendeces(Attendence attendence);
    }
}
