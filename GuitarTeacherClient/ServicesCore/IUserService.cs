﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    interface IUserService
    {
        Task<User> Login(LoginObject loginData);
        Task<User> GetUser();
        Task<string> AddUser(User user);
        Task<string> EditUser(User userToEdit);
        Task<string> EditPassword(LoginObject newPassword);

    }
}
