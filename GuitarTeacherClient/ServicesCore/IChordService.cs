﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.ServicesCore
{
    public interface IChordService
    {
        Task<string> AddChord(Chord chord);
        Task<ChordsList> GetChords();
        Task<ChordsList> GetPublicChords();
        Task<Chord> GetChord(int chordId);
        Task<string> DeleteChord(int chordId);
        Task<string> ShareChord(ShareObject chordToShare);
    }
}
