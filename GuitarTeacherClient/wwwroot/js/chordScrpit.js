
let numberOfStrings = 6

function ChangeImageForStringButton(buttonid) {
    let buttonReference = document.getElementById(buttonid)

    if (buttonReference.classList.contains("cross")) {
        buttonReference.classList.remove("cross")
        buttonReference.classList.add("circle")
    }
    else if (buttonReference.classList.contains("circle")) {
        buttonReference.classList.remove("circle")
    }
    else {
        buttonReference.classList.add("cross")
    }
}

function ChangeImageForFingerButton(buttonid) {
    let buttonReference = document.getElementById(buttonid)

    if (buttonReference.classList.contains("barre-left-end") || buttonReference.classList.contains("barre-right-end") || buttonReference.classList.contains("barre-middle")) {
        return
    }

    if (buttonReference.classList.contains("finger")) {
        buttonReference.classList.remove("finger")
    }
    else {
        buttonReference.classList.add("finger")
    }
}

function ChangeIntoBarre(fretId) {

    var reset = false
    var leftEnd = -1
    var rightEnd = -1

    for (var i = 0; i < numberOfStrings; i++) {

        var stringId = fretId + "string" + i
        var buttonReference = document.getElementById(stringId)

        if (buttonReference.classList.contains("finger")) {

            if (leftEnd == -1) {
                leftEnd = i
            }
            else {
                rightEnd = i
            }

        }

        if (buttonReference.classList.contains("barre-left-end") || buttonReference.classList.contains("barre-right-end") || buttonReference.classList.contains("barre-middle")) {
            reset = true
            break
        }
    }

    if (reset == false) {

        if (leftEnd == -1 || rightEnd == -1) {
            return
        }

        for (var i = leftEnd; i < rightEnd + 1; i++) {

            var stringId = fretId + "string" + i
            var buttonReference = document.getElementById(stringId)

            if (i == leftEnd) {

                if (buttonReference.classList.contains("finger")) {
                    buttonReference.classList.remove("finger")
                }

                buttonReference.classList.add("barre-left-end")

            }
            else if (i == rightEnd) {

                if (buttonReference.classList.contains("finger")) {
                    buttonReference.classList.remove("finger")
                }

                buttonReference.classList.add("barre-right-end")

            }
            else {

                if (buttonReference.classList.contains("finger")) {
                    buttonReference.classList.remove("finger")
                }

                buttonReference.classList.add("barre-middle");

            }
        }

    }
    else {
        for (var i = 0; i < numberOfStrings; i++) {

            var stringId = fretId + "string" + i
            var buttonReference = document.getElementById(stringId)

            if (buttonReference.classList.contains("barre-middle")) {
                buttonReference.classList.remove("barre-middle")
            }
            else if (buttonReference.classList.contains("barre-left-end")) {
                buttonReference.classList.remove("barre-left-end")
            }
            else if (buttonReference.classList.contains("barre-right-end")) {
                buttonReference.classList.remove("barre-right-end")
            }
        }
    }
}

function GetFingerSymbol(stringId) {

    var buttonReference = document.getElementById(stringId)

    if (buttonReference.classList.contains("finger")) {
        return 'f'
    }
    else if (buttonReference.classList.contains("barre-middle")) {
        return 'm'
    }
    else if (buttonReference.classList.contains("barre-left-end")) {
        return 'l'
    }
    else if (buttonReference.classList.contains("barre-right-end")) {
        return 'r'
    }
    else {
        return 'b'
    }

}

function GetStringButtonSymbol(stringButtonId) {

    var buttonReference = document.getElementById(stringButtonId)

    if (buttonReference.classList.contains("cross")) {
        return 'x'
    }
    else if (buttonReference.classList.contains("circle")) {
        return 'o'
    }
    else {
        return 'b'
    }
}

function ChangerFretNumber(fretNumber, buttonId) {

    var buttonReference = document.getElementById(buttonId)
    var fretClassName = "fret-" + fretNumber;
    if (fretClassName == "fret-1")
        fretClassName = "blank";

    if (buttonReference.classList.contains("blank")) {
        buttonReference.classList.replace("blank", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-2")) {
        buttonReference.classList.replace("fret-2", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-3")) {
        buttonReference.classList.replace("fret-3", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-4")) {
        buttonReference.classList.replace("fret-4", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-5")) {
        buttonReference.classList.replace("fret-5", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-6")) {
        buttonReference.classList.replace("fret-6", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-7")) {
        buttonReference.classList.replace("fret-7", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-8")) {
        buttonReference.classList.replace("fret-8", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-9")) {
        buttonReference.classList.replace("fret-9", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-10")) {
        buttonReference.classList.replace("fret-10", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-11")) {
        buttonReference.classList.replace("fret-11", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-12")) {
        buttonReference.classList.replace("fret-12", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-13")) {
        buttonReference.classList.replace("fret-13", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-14")) {
        buttonReference.classList.replace("fret-14", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-15")) {
        buttonReference.classList.replace("fret-15", fretClassName)
    }
    else if (buttonReference.classList.contains("fret-16")) {
        buttonReference.classList.replace("fret-16", fretClassName)
    }
}

function GetFretNumber(buttonId) {
    var buttonReference = document.getElementById(buttonId)

    if (buttonReference.classList.contains("blank")) {
        return 1
    }
    else if (buttonReference.classList.contains("fret-2")) {
        return 2
    }
    else if (buttonReference.classList.contains("fret-3")) {
        return 3
    }
    else if (buttonReference.classList.contains("fret-4")) {
        return 4
    }
    else if (buttonReference.classList.contains("fret-5")) {
        return 5
    }
    else if (buttonReference.classList.contains("fret-6")) {
        return 6
    }
    else if (buttonReference.classList.contains("fret-7")) {
        return 7
    }
    else if (buttonReference.classList.contains("fret-8")) {
        return 8
    }
    else if (buttonReference.classList.contains("fret-9")) {
        return 9
    }
    else if (buttonReference.classList.contains("fret-10")) {
        return 10
    }
    else if (buttonReference.classList.contains("fret-11")) {
        return 11
    }
    else if (buttonReference.classList.contains("fret-12")) {
        return 12
    }
    else if (buttonReference.classList.contains("fret-13")) {
        return 13
    }
    else if (buttonReference.classList.contains("fret-14")) {
        return 14
    }
    else if (buttonReference.classList.contains("fret-15")) {
        return 15
    }
    else if (buttonReference.classList.contains("fret-16")) {
        return 16
    }
}

function DownloadChord(chordname, filepath) {

    var filename = chordname + ".png";
    saveAs(filepath, filename)
}