﻿using Blazored.SessionStorage;
using GuitarTeacherClient.Data;
using GuitarTeacherClient.ServicesCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Services
{
    public class StudentService: IStudentService
    {
        private readonly ISessionStorageService sessionStorageService;
        private readonly HttpClient httpClient;

        public StudentService(ISessionStorageService sessionStorageService,
                              HttpClient httpClient)
        {
            this.sessionStorageService = sessionStorageService;
            this.httpClient = httpClient;
        }
        public async Task<Student> GetStudentByLogin(string login)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, login);
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Student>(responseBody);
            }

            throw new Exception(responseBody);
        }
    }
}
