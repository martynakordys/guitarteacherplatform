﻿using Blazored.SessionStorage;
using GuitarTeacherClient.Data;
using GuitarTeacherClient.ServicesCore;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.DataProtection.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Services
{
    public class UserService : IUserService
    {
        private readonly HttpClient httpClient;
        private readonly ISessionStorageService sessionStorageService;

        public UserService(HttpClient httpClient,
                           ISessionStorageService sessionStorageService)
        {
            this.httpClient = httpClient;
            this.sessionStorageService = sessionStorageService;
        }

        public async Task<string> AddUser(User user)
        {
            string serializedObject = JsonConvert.SerializeObject(user);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "");

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");;

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();


            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            else
            {
                throw new Exception(responseBody);
            }

        }

        public async Task<User> GetUser()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<User>(responseBody);
            }

            throw new Exception(responseBody);
        }

        public async Task<User> Login(LoginObject loginData)
        {
            string serializedObject = JsonConvert.SerializeObject(loginData);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "login");

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<User>(responseBody);
            }

            var userError = new User();
            userError.ErrorMessage = responseBody;

            return userError;
        }

        public async Task<string> EditUser(User userToEdit)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(userToEdit);

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);


            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();


            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            else
            {
                throw new Exception(responseBody);
            }

        }

        public async Task<string> EditPassword(LoginObject newPassword)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(newPassword);

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "password");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();


            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            else
            {
                throw new Exception(responseBody);
            }

        }
    }
}
