﻿using Blazored.SessionStorage;
using GuitarTeacherClient.Data;
using GuitarTeacherClient.ServicesCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Services
{
    public class GroupClassService : IGroupClassService
    {
        private readonly ISessionStorageService sessionStorageService;
        private readonly HttpClient httpClient;

        public GroupClassService(ISessionStorageService sessionStorageService,
                                 HttpClient httpClient)
        {
            this.sessionStorageService = sessionStorageService;
            this.httpClient = httpClient;
        }
        public async Task<string> AddClasses(GroupClass classObject)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            classObject.Duration = int.Parse(classObject.DurationString);

            string serializedObject = JsonConvert.SerializeObject(classObject);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }

        public async Task<GroupClassesList> GetUnrealizedClasses()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<GroupClassesList>(responseBody);
            }
            else
            {
                throw new Exception(responseBody);
            }
        }
        public async Task<GroupClassesList> GetRealizedClasses()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "history");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<GroupClassesList>(responseBody);
            }

            throw new Exception("BadRequest!");
        }
        public async Task<GroupClass> GetClass(int id)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, id.ToString());
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<GroupClass>(responseBody);
            }

            throw new Exception("BadRequest!");
        }
        public async Task<string> RealizeClass(Attendence attendence)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "teacher/realize");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            string serializedObject = JsonConvert.SerializeObject(attendence);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }
        public async Task<string> EditClass(GroupClass groupClass)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            groupClass.Duration = int.Parse(groupClass.DurationString);

            string serializedObject = JsonConvert.SerializeObject(groupClass);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }
        public async Task<string> DeleteClass(int id)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Delete, "teacher/" + id.ToString());
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }

        public async Task<string> EditAttendeces(Attendence attendence)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "teacher/attendence");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            string serializedObject = JsonConvert.SerializeObject(attendence);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }

    }
}
