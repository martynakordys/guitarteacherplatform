﻿using Blazored.SessionStorage;
using GuitarTeacherClient.Data;
using GuitarTeacherClient.ServicesCore;
using MatBlazor;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Services
{
    public class IndividualClassService: IIndividualClassService
    {
        private readonly HttpClient httpClient;
        private readonly ISessionStorageService sessionStorageService;

        public IndividualClassService(HttpClient httpClient,
            ISessionStorageService sessionStorageService)
        {
            this.httpClient = httpClient;
            this.sessionStorageService = sessionStorageService;
        }
        public async Task<string> AddClasses(IndividualClass classObject)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            classObject.DurationTime = int.Parse(classObject.DurationTimeString);


            string serializedObject = JsonConvert.SerializeObject(classObject);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "/teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            if(response.IsSuccessStatusCode)
            {
                return "";
            }
            else
            {
                return await response.Content.ReadAsStringAsync();
            }
        }

        public async Task<IndividualClassesList> GetUnrealizedClasses()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<IndividualClassesList>(responseBody);
            }

            throw new Exception("BadRequest!");
        }

        public async Task<IndividualClassesList> GetRealizedClass()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "history");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<IndividualClassesList>(responseBody);
            }

            throw new Exception("BadRequest!");
        }

        public async Task<string> EditClasses(IndividualClass classObject)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            classObject.DurationTime = int.Parse(classObject.DurationTimeString);

            string serializedObject = JsonConvert.SerializeObject(classObject);

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            if (response.IsSuccessStatusCode)
            {
                return "";
            }
            else
            {
                return await response.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> DeleteClasses(int id)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Delete, "teacher/"+id.ToString());
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            if (response.IsSuccessStatusCode)
            {
                return "";
            }
            else
            {
                return await response.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> RealizeClasses(IndividualClass classObject)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(classObject);

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "teacher/realize");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            if (response.IsSuccessStatusCode)
            {
                return "";
            }
            else
            {
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
