﻿using GuitarTeacherClient.Data;
using GuitarTeacherClient.ServicesCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Services
{
    public class UserRoleService : IUserRoleService
    {
        private readonly HttpClient httpClient;

        public UserRoleService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<UserRolesList> GetAvailableRoles()
        {
            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<UserRolesList>(responseBody);
        }
    }
}
