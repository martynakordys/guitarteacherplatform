﻿using GuitarTeacherClient.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazored.SessionStorage;
using System.Net.Http;
using Newtonsoft.Json;
using GuitarTeacherClient.ServicesCore;

namespace GuitarTeacherClient.Services
{
    public class ChordService: IChordService
    {
        private readonly ISessionStorageService sessionStorageService;
        private readonly HttpClient httpClient;

        public ChordService(ISessionStorageService sessionStorageService,
                            HttpClient httpClient)
        {
            this.sessionStorageService = sessionStorageService;
            this.httpClient = httpClient;
        }
        public async Task<string> AddChord(Chord chord)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(chord);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }

        public async Task<ChordsList> GetChords()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<ChordsList>(responseBody);
            }
            else
            {
                throw new Exception(responseBody);
            }
        }

        public async Task<string> DeleteChord(int chordId)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Delete, "teacher/"+chordId.ToString());
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }

        public async Task<string> ShareChord(ShareObject chordToShare)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(chordToShare);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "teacher/share");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            throw new Exception(responseBody);
        }

        public async Task<ChordsList> GetPublicChords()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "public");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<ChordsList>(responseBody);
            }
            else
            {
                throw new Exception(responseBody);
            }
        }

        public async Task<Chord> GetChord(int chordId)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, chordId.ToString());
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Chord>(responseBody);
            }
            else
            {
                throw new Exception(responseBody);
            }
        }
    }
}
