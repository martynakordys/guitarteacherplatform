﻿using Blazored.SessionStorage;
using GuitarTeacherClient.Data;
using GuitarTeacherClient.ServicesCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Services
{
    public class EvaluationService : IEvaluationService
    {
        private readonly ISessionStorageService sessionStorageService;
        private readonly HttpClient httpClient;

        public EvaluationService(ISessionStorageService sessionStorageService,
                                 HttpClient httpClient)
        {
            this.sessionStorageService = sessionStorageService;
            this.httpClient = httpClient;
        }

        public async Task<string> AddEvaluation(Evaluation evaluationToAdd)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(evaluationToAdd);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            throw new Exception(responseBody);
        }

        public async Task<string> DeleteEvaluation(int idToDelete)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Delete, "teacher/"+idToDelete.ToString());
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }
            else
            {
                throw new Exception(responseBody);
            }
        }

        public async Task<string> EditEvaluation(Evaluation evaluationToEdit)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(evaluationToEdit);

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            throw new Exception(responseBody);
        }

        public async Task<EvaluationsList> GetEvaluations()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<EvaluationsList>(responseBody);
            }
            else
            {
                throw new Exception(responseBody);
            }
        }
    }
}
