﻿using Blazored.SessionStorage;
using GuitarTeacherClient.Data;
using GuitarTeacherClient.ServicesCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Services
{
    public class TaskService : ITaskService
    {
        private readonly HttpClient httpClient;
        private readonly ISessionStorageService sessionStorageService;

        public TaskService(HttpClient httpClient,
                           ISessionStorageService sessionStorageService)
        {
            this.httpClient = httpClient;
            this.sessionStorageService = sessionStorageService;
        }

        public async Task<string> AddClass(TaskObj toAdd)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(toAdd);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            throw new Exception(responseBody);
        }

        public async Task<string> DeleteTask(int idToDelete)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Delete, "teacher/"+idToDelete.ToString());
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            throw new Exception(responseBody);
        }

        public async Task<string> EditTaskContent(TaskObj toEdit)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(toEdit);

            var httpRequest = new HttpRequestMessage(HttpMethod.Patch, "teacher");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            throw new Exception(responseBody);
        }

        public async Task<string> EvaluateTask(TaskObj toEvaluate)
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            string serializedObject = JsonConvert.SerializeObject(toEvaluate);

            var httpRequest = new HttpRequestMessage(HttpMethod.Post, "teacher/evaluate");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            httpRequest.Content = new StringContent(serializedObject);
            httpRequest.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); ;

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return responseBody;
            }

            throw new Exception(responseBody);
        }

        public async Task<TasksList> GetRelizedTasks()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "history");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<TasksList>(responseBody);
            }

            throw new Exception(responseBody);
        }

        public async Task<TasksList> GetUnrealizedTasks()
        {
            var token = await sessionStorageService.GetItemAsync<string>("token");

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, "");
            httpRequest.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.SendAsync(httpRequest);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<TasksList>(responseBody);
            }

            throw new Exception(responseBody);
        }
    }
}
