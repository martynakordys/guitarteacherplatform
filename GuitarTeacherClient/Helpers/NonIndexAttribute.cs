﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GuitarTeacherClient.Helpers
{
    public class NonIndexAttribute: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value==null||value.ToString() == "NON")
            {
                return false;
            }
            return true;
        }
    }
}
