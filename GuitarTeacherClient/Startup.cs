using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.SessionStorage;
using GuitarTeacherClient.Helpers;
using GuitarTeacherClient.Services;
using GuitarTeacherClient.ServicesCore;
using MatBlazor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GuitarTeacherClient
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            services.AddBlazoredSessionStorage();

            services.AddTransient<ValidateHeaderHandler>();

            services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();

            services.AddMatToaster(config =>
            {
                config.Position = MatToastPosition.BottomRight;
                config.PreventDuplicates = true;
                config.NewestOnTop = true;
                config.ShowCloseButton = true;
                config.MaximumOpacity = 95;
                config.VisibleStateDuration = 3000;
                config.Position = MatToastPosition.TopCenter;
            });

            services.AddHttpClient<IUserService, UserService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/users/");
            });

            services.AddHttpClient<ITaskService, TaskService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/tasks/");
            });

            services.AddHttpClient<IEvaluationService, EvaluationService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/evaluations/");
            });

            services.AddHttpClient<IUserRoleService, UserRoleService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/userRoles");
            });

            services.AddHttpClient<IIndividualClassService, IndividualClassService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/individualClasses/");
            });

            services.AddHttpClient<IGroupClassService, GroupClassService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/groupClasses/");
            });

            services.AddHttpClient<IStudentService, StudentService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/students/");
            });

            services.AddHttpClient<IChordService, ChordService>(x =>
            {
                x.BaseAddress = new Uri("http://127.0.0.1:8080/chords/");
            });

            if (!services.Any(x => x.ServiceType == typeof(HttpClient)))
            {
                // Setup HttpClient for server side in a client side compatible fashion
                services.AddScoped<HttpClient>(s =>
                {
                    // Creating the URI helper needs to wait until the JS Runtime is initialized, so defer it.
                    var uriHelper = s.GetRequiredService<NavigationManager>();
                    return new HttpClient
                    {
                        BaseAddress = new Uri(uriHelper.BaseUri)
                    };
                });
            }

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
