#pragma checksum "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c146cb9608e1b8c18978371c85c9311694d41319"
// <auto-generated/>
#pragma warning disable 1591
namespace GuitarTeacherClient.Pages.Teacher.GroupClasses
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using GuitarTeacherClient;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using GuitarTeacherClient.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using MatBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
using GuitarTeacherClient.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
using GuitarTeacherClient.ServicesCore;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/teacher/GroupClasses/history")]
    public partial class GroupClassHistory : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<style>
    .container-header {
        border-left: 6px solid #1E6B94;
        padding-left: 10px;
    }

    .container {
        border: 1px solid #B1D9EE;
        border-radius: 10px;
    }

    .no-margin-or-padding {
        padding: 0px;
        margin: 0px;
    }

    .table {
        border-radius: 10px;
    }

        .table th {
            background-color: #377EA4;
            color: #FFF;
            font-weight: bolder;
            text-align: center;
            padding: 12px;
        }

        .table td {
            text-align: center;
            padding: 12px;
            vertical-align: middle;
        }
</style>

");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "col-md-10 my-4 mx-5");
            __builder.AddMarkupContent(3, "\r\n    ");
            __builder.AddMarkupContent(4, "<div class=\"py-2 container-header\">\r\n        <h1>Group classes:</h1>\r\n    </div>\r\n    ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "my-4");
            __builder.AddMarkupContent(7, "\r\n\r\n        ");
            __Blazor.GuitarTeacherClient.Pages.Teacher.GroupClasses.GroupClassHistory.TypeInference.CreateMatTable_0(__builder, 8, 9, "DateOfClasses", 10, 
#nullable restore
#line 50 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                                                classesList.GroupClasses

#line default
#line hidden
#nullable disable
            , 11, "mat-elevation-z5 table", 12, 
#nullable restore
#line 50 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                                                                                                                     false

#line default
#line hidden
#nullable disable
            , 13, 
#nullable restore
#line 50 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                                                                                                                                      1000

#line default
#line hidden
#nullable disable
            , 14, (__builder2) => {
                __builder2.AddMarkupContent(15, "\r\n                ");
                __builder2.AddMarkupContent(16, "<th>\r\n                    <div style=\"width:200px\">\r\n                        Date of class\r\n                    </div>\r\n                </th>\r\n                ");
                __builder2.AddMarkupContent(17, "<th>\r\n                    <div style=\"width:75px\">\r\n                        Duration\r\n                    </div>\r\n                </th>\r\n                ");
                __builder2.AddMarkupContent(18, "<th>\r\n                    <div style=\"width:275px\">\r\n                        Attendences\r\n                    </div>\r\n                </th>\r\n                ");
                __builder2.AddMarkupContent(19, "<th>Topic</th>\r\n            ");
            }
            , 20, (context) => (__builder2) => {
                __builder2.AddMarkupContent(21, "\r\n                ");
                __builder2.OpenElement(22, "td");
                __builder2.AddContent(23, 
#nullable restore
#line 70 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                     context.DateOfClass.ToString("dd-MM-yyyy   HH:mm")

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(24, "\r\n                ");
                __builder2.OpenElement(25, "td");
                __builder2.AddContent(26, 
#nullable restore
#line 71 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                     context.Duration

#line default
#line hidden
#nullable disable
                );
                __builder2.AddContent(27, " min");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(28, "\r\n                ");
                __builder2.OpenElement(29, "td");
                __builder2.AddAttribute(30, "style", "display: flex; align-items: center; justify-content:center");
                __builder2.AddMarkupContent(31, "\r\n                    Number of students: ");
                __builder2.AddContent(32, 
#nullable restore
#line 73 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                                         context.Students.Count

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(33, "\r\n                    ");
                __builder2.OpenComponent<MatBlazor.MatIconButton>(34);
                __builder2.AddAttribute(35, "Style", "outline:none; color:#1E6B94;");
                __builder2.AddAttribute(36, "Icon", "format_list_bulleted");
                __builder2.AddAttribute(37, "OnClick", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 74 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                                                                                                             ()=> { itemToViewStudents = context; viewStudentsDialogIsOpen = true; }

#line default
#line hidden
#nullable disable
                )));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(38, "\r\n                    ");
                __builder2.OpenComponent<MatBlazor.MatIconButton>(39);
                __builder2.AddAttribute(40, "Style", "outline:none; color:#1E6B94;");
                __builder2.AddAttribute(41, "Icon", "edit");
                __builder2.AddAttribute(42, "OnClick", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 75 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                                                                                             ()=>EditAttendecesClicked(context)

#line default
#line hidden
#nullable disable
                )));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(43, "\r\n                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(44, "\r\n                ");
                __builder2.OpenElement(45, "td");
                __builder2.AddAttribute(46, "style", "padding: 6px;");
                __builder2.AddMarkupContent(47, "\r\n                    ");
                __builder2.AddContent(48, 
#nullable restore
#line 78 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                     context.Topic

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(49, "\r\n                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(50, "\r\n            ");
            }
            );
            __builder.AddMarkupContent(51, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(52, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(53, "\r\n\r\n");
            __builder.OpenComponent<MatBlazor.MatDialog>(54);
            __builder.AddAttribute(55, "IsOpen", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 85 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                         viewStudentsDialogIsOpen

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(56, "IsOpenChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.Boolean>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.Boolean>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => viewStudentsDialogIsOpen = __value, viewStudentsDialogIsOpen))));
            __builder.AddAttribute(57, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.AddMarkupContent(58, "\r\n    ");
                __builder2.OpenComponent<MatBlazor.MatDialogTitle>(59);
                __builder2.AddAttribute(60, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.AddMarkupContent(61, "\r\n        Group\r\n    ");
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(62, "\r\n    ");
                __builder2.OpenComponent<MatBlazor.MatDialogContent>(63);
                __builder2.AddAttribute(64, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.AddMarkupContent(65, "\r\n        ");
                    __builder3.OpenComponent<MatBlazor.MatList>(66);
                    __builder3.AddAttribute(67, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddMarkupContent(68, "\r\n");
#nullable restore
#line 91 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
             foreach (var student in itemToViewStudents.Students)
            {

#line default
#line hidden
#nullable disable
                        __builder4.AddContent(69, "                ");
                        __builder4.OpenComponent<MatBlazor.MatListItem>(70);
                        __builder4.AddAttribute(71, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder5) => {
                            __builder5.AddMarkupContent(72, "\r\n                    ");
                            __builder5.OpenComponent<MatBlazor.MatListItemText>(73);
                            __builder5.AddAttribute(74, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder6) => {
                                __builder6.AddMarkupContent(75, "\r\n                        ");
                                __builder6.OpenComponent<MatBlazor.MatListItemPrimaryText>(76);
                                __builder6.AddAttribute(77, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder7) => {
                                    __builder7.AddMarkupContent(78, "\r\n                            ");
                                    __builder7.AddContent(79, 
#nullable restore
#line 96 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                             student.Name

#line default
#line hidden
#nullable disable
                                    );
                                    __builder7.AddMarkupContent(80, "\r\n                        ");
                                }
                                ));
                                __builder6.CloseComponent();
                                __builder6.AddMarkupContent(81, "\r\n");
#nullable restore
#line 98 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                         if (student.IsPresent)
                        {

#line default
#line hidden
#nullable disable
                                __builder6.AddContent(82, "                            ");
                                __builder6.OpenComponent<MatBlazor.MatListItemSecondaryText>(83);
                                __builder6.AddAttribute(84, "Style", "justify-content: right");
                                __builder6.AddAttribute(85, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder7) => {
                                    __builder7.AddMarkupContent(86, "\r\n                                was present\r\n                            ");
                                }
                                ));
                                __builder6.CloseComponent();
                                __builder6.AddMarkupContent(87, "\r\n");
#nullable restore
#line 103 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
                                __builder6.AddContent(88, "                            ");
                                __builder6.OpenComponent<MatBlazor.MatListItemSecondaryText>(89);
                                __builder6.AddAttribute(90, "Style", "justify-items: right");
                                __builder6.AddAttribute(91, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder7) => {
                                    __builder7.AddMarkupContent(92, "\r\n                                was absent\r\n                            ");
                                }
                                ));
                                __builder6.CloseComponent();
                                __builder6.AddMarkupContent(93, "\r\n");
#nullable restore
#line 109 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
                        }

#line default
#line hidden
#nullable disable
                                __builder6.AddContent(94, "                    ");
                            }
                            ));
                            __builder5.CloseComponent();
                            __builder5.AddMarkupContent(95, "\r\n                ");
                        }
                        ));
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(96, "\r\n");
#nullable restore
#line 112 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
            }

#line default
#line hidden
#nullable disable
                        __builder4.AddMarkupContent(97, "            <br>\r\n        ");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(98, "\r\n    ");
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(99, "\r\n");
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 119 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\GroupClasses\GroupClassHistory.razor"
       

    GroupClassesList classesList = new GroupClassesList();
    GroupClass itemToViewStudents = new GroupClass();

    bool viewStudentsDialogIsOpen = false;

    string topic = null;

    protected async override Task OnInitializedAsync()
    {
        classesList = await classService.GetRealizedClasses();

        base.OnInitialized();
    }

    void EditAttendecesClicked(GroupClass item)
    {
        viewStudentsDialogIsOpen = false;
        navigationManager.NavigateTo("/teacher/groupClass/editAttendences/"+item.Id.ToString());
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IGroupClassService classService { get; set; }
    }
}
namespace __Blazor.GuitarTeacherClient.Pages.Teacher.GroupClasses.GroupClassHistory
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateMatTable_0<TableItem>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.Collections.Generic.IEnumerable<TableItem> __arg1, int __seq2, System.Object __arg2, int __seq3, global::System.Boolean __arg3, int __seq4, global::System.Int32 __arg4, int __seq5, global::Microsoft.AspNetCore.Components.RenderFragment __arg5, int __seq6, global::Microsoft.AspNetCore.Components.RenderFragment<TableItem> __arg6)
        {
        __builder.OpenComponent<global::MatBlazor.MatTable<TableItem>>(seq);
        __builder.AddAttribute(__seq0, "SortBy", __arg0);
        __builder.AddAttribute(__seq1, "Items", __arg1);
        __builder.AddAttribute(__seq2, "class", __arg2);
        __builder.AddAttribute(__seq3, "ShowPaging", __arg3);
        __builder.AddAttribute(__seq4, "PageSize", __arg4);
        __builder.AddAttribute(__seq5, "MatTableHeader", __arg5);
        __builder.AddAttribute(__seq6, "MatTableRow", __arg6);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
