#pragma checksum "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\Tasks\TaskView.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ad6196057774f1355008cd0ad1949897343af53c"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace GuitarTeacherClient.Pages.Teacher.Tasks
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using GuitarTeacherClient;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using GuitarTeacherClient.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\_Imports.razor"
using MatBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\Tasks\TaskView.razor"
using GuitarTeacherClient.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\Tasks\TaskView.razor"
using GuitarTeacherClient.ServicesCore;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/teacher/tasks")]
    [Microsoft.AspNetCore.Components.RouteAttribute("/teacher/tasks/{notificationType}")]
    public partial class TaskView : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 139 "C:\Users\HP\Desktop\pracaINZ\GitRepo\guitarteacherplatform\GuitarTeacherClient\Pages\Teacher\Tasks\TaskView.razor"
       

    [Parameter]
    public string notificationType { get; set; }

    TasksList tasks = new TasksList();

    bool showBlankRow = false;
    bool evaluateDialogIsOpen = false;
    bool deleteDialogIsOpen = false;
    bool editDialogIsOpen = false;

    TaskObj itemToEdit = new TaskObj();

    int note = 0;
    int idToEvaluate = 0;
    int idToDelete = 0;


    protected async override Task OnInitializedAsync()
    {
        tasks = await taskService.GetUnrealizedTasks();

        if (tasks.Tasks.Count == 0)
        {
            showBlankRow = true;
            tasks.Tasks.Add(new TaskObj());
        }

        if (notificationType == "delete")
        {
            toaster.Add("Task deleted successfully", MatToastType.Info, icon: "offline_pin", configure: config =>
            {
                config.HideTransitionDuration = 200;
                config.RequireInteraction = true;
                config.ShowCloseButton = false;
            });
        }
        else if (notificationType == "edit")
        {
            toaster.Add("Task edited successfully", MatToastType.Info, icon: "offline_pin", configure: config =>
            {
                config.HideTransitionDuration = 200;
                config.RequireInteraction = true;
                config.ShowCloseButton = false;
            });
        }
        else if (notificationType == "realize")
        {
            toaster.Add("Task evluated successfully", MatToastType.Info, icon: "offline_pin", configure: config =>
            {
                config.HideTransitionDuration = 200;
                config.RequireInteraction = true;
                config.ShowCloseButton = false;
            });
        }

    }

    private void EditClicked(TaskObj item)
    {
        itemToEdit = item;
        editDialogIsOpen = true;
    }

    private void EditTask()
    {
        taskService.EditTaskContent(itemToEdit);

        navigationManager.NavigateTo("/teacher/tasks/edit", forceLoad: true);
    }

    private void DeleteClicked(TaskObj item)
    {
        idToDelete = item.Id;
        deleteDialogIsOpen = true;
    }

    private void DeleteTask()
    {
        taskService.DeleteTask(idToDelete);
        idToDelete = 0;
        navigationManager.NavigateTo("/teacher/tasks/delete", forceLoad: true);
    }

    private void EvaluateClicked(TaskObj item)
    {
        idToEvaluate = item.Id;
        evaluateDialogIsOpen = true;
    }

    private void EvaluateTask()
    {
        taskService.EvaluateTask(new TaskObj
        {
            Id = idToEvaluate,
            Note = note
        });
        note = 0;
        idToEvaluate = 0;
        navigationManager.NavigateTo("/teacher/tasks/evaluate", forceLoad: true);
    }



#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IMatToaster toaster { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ITaskService taskService { get; set; }
    }
}
#pragma warning restore 1591
